package com.Fisherman.Greekwpa;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class GreekWpaApp extends Application
{

    public synchronized Tracker getTracker()
	{
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
		return analytics.newTracker(R.xml.app_global_tracker);
    }

}
