package com.Fisherman.Greekwpa;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.Fisherman.Greekwpa.Receivers.AutoConnectReceiver;
import com.Fisherman.Greekwpa.Receivers.ProgressReceiver;
import com.Fisherman.Greekwpa.Receivers.WiFiScanReceiver;
import com.Fisherman.Greekwpa.connector.WiFi;
import com.Fisherman.Greekwpa.utils.WifiUtil;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;

public class AutoConnectService extends Service implements WiFiScanReceiver.IGetScannedResults, AutoConnectReceiver.IAutoConnectListener
{
    private WifiManager wifi;
    private ArrayList<String> tempKeys;
    private List<ScanResult> scanResults;
    private String desiredMac;
    private BroadcastReceiver scanReceiver = null;
    private int currentNetworkId;
    private String tryingKey = "";
    private AutoConnectReceiver autoReceiver;
    private Context ctx;
    private Handler handler;
    private final int MAX_SIGNAL_LEVEL = -83;
    private Intent pgIntent;
    private ScanResult mResult;

    @SuppressLint("InlinedApi")
    @Override
    public void onCreate()
    {
        super.onCreate();
        this.wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        autoReceiver = AutoConnectReceiver.newInstance(this);
        ctx = this.getApplicationContext();
        handler = new Handler();
        pgIntent = new Intent(ProgressReceiver.FilterName);
        Log.w(AutoConnectService.class.getName(), "CREATEEEE");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.w(AutoConnectService.class.getName(), "onStartCommanddddd");
        if (intent == null)
        {
            stopSelf();
            return Service.START_NOT_STICKY;
        }
        this.currentNetworkId = -1;
        this.tempKeys = intent.getStringArrayListExtra("keys");
        this.desiredMac = intent.getStringExtra("desiredMac");

        if (this.tempKeys == null || this.desiredMac == null || this.tempKeys.isEmpty())
        {
            handleToasts(R.string.unknownerror, "mac or keys are empty");
            stopSelf();
            return Service.START_NOT_STICKY;
        }

        if (scanReceiver == null)
        {
            try
            {
                scanReceiver = WiFiScanReceiver.newInstance(this);
                registerReceiver(scanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            }
            catch (Exception e)
            {
                WiFi.reenableAllHotspots(wifi);
                stopSelf();
                return Service.START_NOT_STICKY;
            }
            if (WifiUtil.enableWifiAndScan(this, wifi))
            {
                wifi.disconnect();
            }
            else
            {
                WiFi.reenableAllHotspots(wifi);
                stopSelf();
                return Service.START_NOT_STICKY;
            }
        }
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d(AutoConnectService.class.getName(), "Unregister receivers");
        ArrayList<AutoConnectReceiver> autoConnectReceiver = AutoConnectReceiver.getInstances();
        if (autoReceiver != null)
        {
            for (AutoConnectReceiver auto : autoConnectReceiver)
            {
                try
                {
                    unregisterReceiver(auto);
                    auto = null;
                }
                catch (Exception e)
                {
                }
            }
        }
        ArrayList<WiFiScanReceiver> scanReceiver = WiFiScanReceiver.getInstances();
        if (scanReceiver != null)
        {
            for (WiFiScanReceiver scan : scanReceiver)
            {
                try
                {
                    unregisterReceiver(scan);
                    scan = null;
                }
                catch (Exception e)
                {
                }
            }
        }

        LocalBroadcastManager.getInstance(AutoConnectService.this).sendBroadcast(pgIntent);
        WiFi.reenableAllHotspots(wifi);
        wifi.reconnect();
        Log.d(AutoConnectService.class.getName(), "DESTRoY and Reassociate");

    }

    private void handleToasts(final int message, final String key)
    {
        Runnable handleToast = new Runnable()
        {

            @Override
            public void run()
            {
                Toast.makeText(ctx, ctx.getText(message) + " " + key, Toast.LENGTH_LONG).show();
            }
        };
        handler.post(handleToast);
    }

    @Override
    public void getScanResults()
    {
        Log.w(AutoConnectService.class.getName(), "GOT SCAN RESULTS");
        scanResults = wifi.getScanResults();
        if (scanResults == null)
        {
            stopSelf();
            handleToasts(R.string.unknownerror, "scan results is null");
            return;
        }
        try
        {
            unregisterReceiver(scanReceiver); // unregister scan receiver we got the scanResults we wanted
            scanReceiver = null;
        }
        catch (Exception e)
        {
        }
        try
        {
            registerReceiver(autoReceiver, new IntentFilter(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION));
        }
        catch (Exception e)
        {
            handleToasts(R.string.unknownerror, " getScanResults() " + e.toString());
            stopSelf();
            return;
        }

        Thread t = new Thread()
        {
            @Override
            public void run()
            {
                tryingKey = testKey();
                mResult = tryingConnection(scanResults, desiredMac, tryingKey);
            }
        };
        if (wifi.setWifiEnabled(true))
        {
            t.start();
        }
        else
        {
            handleToasts(R.string.toastCantEnable, "");
            stopSelf();
        }
    }

    private synchronized ScanResult tryingConnection(final List<ScanResult> scanResults, final String desiredMac, final String tryingKey)
    {
        String ssid;
        ScanResult result = null;
        if (tryingKey == null)
        {
            Log.d(AutoConnectService.class.getName(), "EMPTY KEYS DUDE");
            stopSelf();
            return null;
        }
        if (scanResults == null)
        {
            Log.d(AutoConnectService.class.getName(), "SCAN RESULTS ARE EMPTY");
            handleToasts(R.string.unknownerror, "Try again");
            stopSelf();
            return null;
        }
        handleToasts(R.string.tryingWith, tryingKey);
        for (int i = 0; i < scanResults.size(); i++)
        {

            if (scanResults.get(i).BSSID.equals(desiredMac))
            {
                Log.d("signal", "level " + scanResults.get(i).level);

                if (scanResults.get(i).level >= MAX_SIGNAL_LEVEL)
                {
                    result = scanResults.get(i);
                    WiFi.cleanPreviousConfiguration(wifi, result);

                    if (wifi.setWifiEnabled(true))
                    {
                        try
                        {
                            currentNetworkId = WiFi.connectToNewNetwork(wifi, scanResults.get(i), tryingKey);
                        }
                        catch (Exception e)
                        {
                            Tracker t = ((GreekWpaApp) getApplication()).getTracker();
                            t.send(new HitBuilders.EventBuilder("AutoConnectService", "ERROR: on connectToNewNetwork").setLabel(e.getMessage()).build());
                            e.printStackTrace();
                            handleToasts(R.string.wifiError, "");
                            stopSelf();
                            return null;
                        }
                    }
                    else
                    {
                        handleToasts(R.string.toastCantEnable, "");
                        stopSelf();
                        return null;
                    }
                }
                else
                {
                    handleToasts(R.string.badSignal, "");
                    stopSelf();
                    return null;
                }

            }

            else if (RouterFilter.compatibleRouter(scanResults, i).equals(RouterFilter.TYPE.THOMSON))
            {
                ssid = scanResults.get(i).SSID;
                ssid = ssid.substring(ssid.length() - 6);

                if (ssid.equals(desiredMac))
                {

                    if (scanResults.get(i).level >= MAX_SIGNAL_LEVEL)
                    {
                        result = scanResults.get(i);
                        WiFi.cleanPreviousConfiguration(wifi, result);

                        if (wifi.setWifiEnabled(true))
                        {
                            try
                            {
                                currentNetworkId = WiFi.connectToNewNetwork(wifi, scanResults.get(i), tryingKey);
                            }
                            catch (Exception e)
                            {
                                Tracker t = ((GreekWpaApp) getApplication()).getTracker();
                                t.send(new HitBuilders.EventBuilder("AutoConnectService", " Exception ERROR: on connectToNewNetwork").setLabel(e.getMessage()).build());
                                handleToasts(R.string.wifiError, "");
                                stopSelf();
                                return null;
                            }
                        }

                        else
                        {
                            handleToasts(R.string.toastCantEnable, "");
                            stopSelf();
                            return null;
                        }

                    }
                    else
                    {
                        handleToasts(R.string.badSignal, "");
                        stopSelf();
                        return null;
                    }
                }
            }
        }
        if(currentNetworkId == -1)
        {
            Tracker t = ((GreekWpaApp) getApplication()).getTracker();
            t.send(new HitBuilders.EventBuilder("AutoConnectService", "Error on network config. currentNetworkId is -1").build());
            handleToasts(R.string.wifiError, "NetWork Config is -1");
            stopSelf();
            return null;
        }
        return result;
    }

    private String testKey()
    {
        if (tempKeys != null && !tempKeys.isEmpty())
        {
            try
            {
                int index = tempKeys.size() - 1;
                String tempKey = tempKeys.get(index);
                tempKeys.remove(index);
                return tempKey;
            }
            catch (IndexOutOfBoundsException e)
            {
                return "";
            }

        }

        return "";

    }

    @Override
    public void onSuccessfulConnection()
    {
        Tracker t = ((GreekWpaApp) getApplication()).getTracker();
        t.send(new HitBuilders.EventBuilder("AutoConnectService", "Connection Succeeded").build());
        stopSelf();
        handleToasts(R.string.connected, tryingKey);
    }

    @Override
    public void onFailedConnection()
    {
        if (tempKeys.isEmpty())
        {
            Tracker t = ((GreekWpaApp) getApplication()).getTracker();
            t.send(new HitBuilders.EventBuilder("AutoConnectService", "Connection Failed").build());
            if (currentNetworkId != -1) wifi.removeNetwork(currentNetworkId);
            handleToasts(R.string.connectionFailed, "");
            stopSelf();
            return;
        }

        handleToasts(R.string.keyFailed, "");

        Thread t = new Thread()
        {
            @Override
            public void run()
            {
                if (mResult == null)
                {
                    Log.d("AutoConnectService","mResult is NULL");
                    handleToasts(R.string.networknotfound, "");
                    stopSelf();
                }
                else
                {
                    tryingKey = testKey();
                    tryingConnection(scanResults, desiredMac, tryingKey);
                    WiFi.changePasswordAndConnect(wifi, mResult, tryingKey);
                }

            }
        };
        if (wifi.setWifiEnabled(true))
        {
            t.start();
        }
        else
        {
            handleToasts(R.string.toastCantEnable, "");
            stopSelf();
        }
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return new LocalBinder();
    }

    private class LocalBinder extends Binder
    {
        AutoConnectService getService()
        {
            return AutoConnectService.this;
        }
    }
}
