package com.Fisherman.Greekwpa;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;

public class InterstitialListener extends AdListener
{
	private final InterstitialAd ad;
	private final ActionBarActivity activity;

	public InterstitialListener(InterstitialAd ad, ActionBarActivity activity)
	{
		this.ad = ad;
		this.activity = activity;
	}

	@Override
	public void onAdLoaded()
	{
		super.onAdLoaded();
		if (ad.isLoaded())
		{
			Log.w("adlistener", "Ad loaded");
			if ((activity != null) && (!activity.isFinishing()))
			{
				Log.w("adlistener", "mpike");
				Fragment frag = activity.getSupportFragmentManager().findFragmentByTag("dialog");
				if (frag == null)
				{
					Log.w("adlistener", "ad should have been shown");
					ad.show();
				}
			}

		}
	}

}
