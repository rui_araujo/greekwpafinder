package com.Fisherman.Greekwpa;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends ArrayAdapter<ScanResult>
{

    private final int[] colors = new int[]{0x30B2B2B2, 0x303399FF};
    private final List<ScanResult> scanResults;
    private final LayoutInflater inflater;

    public MyAdapter(Context context, int resource, List<ScanResult> scanResults)
    {
        super(context, resource, scanResults);
        this.scanResults = scanResults;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // LayoutInflater inflater = (LayoutInflater)
        // context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;
        ViewHolder holder;
        String ssid, bssid;
        if (view == null)
        {
            view = inflater.inflate(R.layout.item_list_wifi, parent, false);
            holder = new ViewHolder();
            holder.txtSsid = (TextView) view.findViewById(R.id.wifiName);
            holder.txtMac = (TextView) view.findViewById(R.id.wifiMAC);
            holder.imgTick = (ImageView) view.findViewById(R.id.imgtick);
            holder.imgSignal = (ImageView) view.findViewById(R.id.imgsignal);
            view.setTag(holder);
        }
        holder = (ViewHolder) view.getTag();

        int signal, convertedSignal;
        int colorPos = position % colors.length;
        view.setBackgroundColor(colors[colorPos]);

        ssid = scanResults.get(position).SSID;
        bssid = scanResults.get(position).BSSID;

        holder.txtSsid.setText(ssid);
        holder.txtMac.setText(bssid);
        signal = scanResults.get(position).level;
        convertedSignal = WifiManager.calculateSignalLevel(signal, 4);
        holder.imgTick.setImageResource(R.drawable.tick);
        holder.imgTick.setVisibility(View.INVISIBLE);

        switch (convertedSignal)
        {
            case 3:
                holder.imgSignal.setImageResource(R.drawable.wifi_very_strong);
                break;
            case 2:
                holder.imgSignal.setImageResource(R.drawable.wifi_strong);
                break;
            case 1:
                holder.imgSignal.setImageResource(R.drawable.wifi_medium);
                break;
            case 0:
                holder.imgSignal.setImageResource(R.drawable.wifi_weak);
                break;
        }

        if (RouterFilter.compatibleRouter(scanResults, position) != RouterFilter.TYPE.OTHER && RouterFilter.compatibleRouter(scanResults, position) != RouterFilter.TYPE.NEWTHOMSON)
        {
            holder.imgTick.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private static class ViewHolder
    {
        private TextView txtSsid;
        private TextView txtMac;
        private ImageView imgTick;
        private ImageView imgSignal;
    }

    public void updateAdapter(List<ScanResult> result)
    {
        this.scanResults.clear();
        this.scanResults.addAll(result);
        notifyDataSetChanged();
    }

}
