package com.Fisherman.Greekwpa.Activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.Fisherman.Greekwpa.Fragments.AboutDialogFrag;
import com.Fisherman.Greekwpa.Fragments.BillingDialogFrag;
import com.Fisherman.Greekwpa.Integrity;
import com.Fisherman.Greekwpa.MyPreferences;
import com.Fisherman.Greekwpa.R;

public abstract class BaseActivity extends ActionBarActivity
{
	protected MenuItem menuRefresh;
	protected MenuItem menuSherlock;
	protected MyPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		prefs = new MyPreferences(this);
		Integrity.checkIntegrity(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.options, menu);
		menuRefresh = menu.findItem(R.id.menuRefresh);
		menuRefresh.setVisible(false);
		menuSherlock = menu.findItem(R.id.menuSherlock);
		return super.onCreateOptionsMenu(menu);
	}

    @Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.action_about)
		{
			showDialog(AboutDialogFrag.newInstance(R.string.about, R.string.aboutdiag));
		}
		else if (item.getItemId() == R.id.action_remove_ads)
		{
			showDialog(BillingDialogFrag.newInstance(R.string.remove_ads, R.string.remove_ads_message));
		}
		else if (item.getItemId() == R.id.menuSherlock)
		{
			startSherlock();
		}

		return super.onOptionsItemSelected(item);
	}

	protected void showDialog(DialogFragment diag)
	{
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		if (diag != null && !diag.isAdded())
			diag.show(ft, "dialog");
	}

	private void startSherlock()
	{
		final String sherlockPackage = "com.fisherman.wifisherlock";

		Intent sherlockIntent = getPackageManager().getLaunchIntentForPackage(sherlockPackage);

		if (sherlockIntent != null)
		{
			sherlockIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(sherlockIntent);
		}
		else
		{
			try
			{
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + sherlockPackage)));
			}
			catch (ActivityNotFoundException e)
			{
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + sherlockPackage)));
			}
		}
	}

	protected void cancelDialog()
	{
		try
		{
			Fragment prevFrag = getSupportFragmentManager().findFragmentByTag("dialog");
			if (prevFrag != null)
			{
				DialogFragment df = (DialogFragment) prevFrag;
				df.dismiss();
				Log.i("BaseActivity cancelDialogMethod", "DISMISSED IT!");

			}
		}
		catch (IllegalStateException e)
		{
		}
	}
}
