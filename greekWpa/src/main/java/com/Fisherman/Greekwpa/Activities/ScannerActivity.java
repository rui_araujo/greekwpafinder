package com.Fisherman.Greekwpa.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.Fisherman.Greekwpa.MyAdapter;
import com.Fisherman.Greekwpa.R;
import com.Fisherman.Greekwpa.Receivers.WiFiScanReceiver;
import com.Fisherman.Greekwpa.RouterFilter;
import com.Fisherman.Greekwpa.utils.WifiUtil;
import com.google.android.gms.analytics.GoogleAnalytics;

import java.util.List;

public class ScannerActivity extends BaseActivity implements WiFiScanReceiver.IGetScannedResults
{

	private List<ScanResult> scanResults;
    private ListView listView;
	private WifiManager wifimanager;
	private Intent intent;
	private BroadcastReceiver receiver;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scan);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		listView = (ListView) findViewById(R.id.listView1);
		intent = new Intent(this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		wifimanager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				String ssid = scanResults.get(arg2).SSID;
				switch (RouterFilter.compatibleRouter(scanResults, arg2))
				{
				case THOMSON:
					intent.putExtra("ROUTERBRAND", ssid.substring(ssid.length() - 6));
					break;
				case NEWTHOMSON:
					intent.putExtra("ROUTERBRAND", ssid.substring(ssid.length() - 6));
					break;
				default:
					intent.putExtra("ROUTERBRAND", scanResults.get(arg2).BSSID);
					break;
				}

				try
				{
					startActivity(intent);
				}
				catch (Exception e)
				{
				}
			}

		});
		Log.w("ScannerActivity", "onCreate");
		registerTheReceiver();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		final boolean result = super.onCreateOptionsMenu(menu);
		Log.w("ScannerActivity", "onCreateOptionsMenu");
		if (result)
		{
			menuSherlock.setVisible(false);
			menuRefresh.setVisible(true);
			setRefreshActionButtonState(WifiUtil.enableWifiAndScan(this, wifimanager));
		}
		return result;
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		unregisterTheReceiver();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	private void registerTheReceiver()
	{
		// Register Broadcast Receiver
		try
		{
			receiver = WiFiScanReceiver.newInstance(this);
			registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		}
		catch (Exception e)
		{
			Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
		}

	}

	private void unregisterTheReceiver()
	{
		try
		{
			unregisterReceiver(receiver);
			receiver = null;
		}
		catch (Exception e)
		{
			Toast.makeText(ScannerActivity.this, e.toString(), Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void getScanResults()
	{
		scanResults = wifimanager.getScanResults();

		if (scanResults != null)
		{
			if (listView.getAdapter() == null)
			{
                MyAdapter arrAdapter = new MyAdapter(this, R.layout.item_list_wifi, scanResults);
				listView.setAdapter(arrAdapter);
			}
			else
			{
				((MyAdapter)listView.getAdapter()).updateAdapter(scanResults);
			}			
		}
		setRefreshActionButtonState(false);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case android.R.id.home:
			startActivity(intent);
			return true;
		case R.id.menuRefresh:
			setRefreshActionButtonState(WifiUtil.enableWifiAndScan(this, wifimanager));
			return true;
		default:
			return super.onOptionsItemSelected(item);

		}
	}

	private void setRefreshActionButtonState(final boolean refreshing)
	{
		if (menuRefresh != null)
		{
			if (refreshing)
			{
				MenuItemCompat.setActionView(menuRefresh, R.layout.actionbar_indeterminate_progress);
			}
			else
			{
				MenuItemCompat.setActionView(menuRefresh, null);
			}
		}
	}

}