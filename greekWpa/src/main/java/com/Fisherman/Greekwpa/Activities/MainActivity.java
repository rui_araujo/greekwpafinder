package com.Fisherman.Greekwpa.Activities;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Fisherman.Greekwpa.Algorithms.KeygenThread;
import com.Fisherman.Greekwpa.AutoConnectService;
import com.Fisherman.Greekwpa.Fragments.AutoConnectButtonFragment;
import com.Fisherman.Greekwpa.Fragments.KeyTextFragment;
import com.Fisherman.Greekwpa.Fragments.ProgressDialogFrag;
import com.Fisherman.Greekwpa.Fragments.ServiceAlertDialogFrag;
import com.Fisherman.Greekwpa.GreekWpaApp;
import com.Fisherman.Greekwpa.Integrity;
import com.Fisherman.Greekwpa.InterstitialListener;
import com.Fisherman.Greekwpa.MessageHandler;
import com.Fisherman.Greekwpa.ProgressBarWrapper;
import com.Fisherman.Greekwpa.R;
import com.Fisherman.Greekwpa.Receivers.ProgressReceiver;
import com.Fisherman.Greekwpa.Receivers.WiFiScanReceiver;
import com.Fisherman.Greekwpa.RouterFactory;
import com.Fisherman.Greekwpa.billingutil.MyBilling;
import com.Fisherman.Greekwpa.billingutil.MyCheckBilling;
import com.Fisherman.Greekwpa.explore.AsyncExploreController;
import com.Fisherman.Greekwpa.models.VenueSearchRoot;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends BaseActivity implements OnClickListener,
        MessageHandler.IActivityHandler, ProgressReceiver.IProgressBar,
        AutoConnectButtonFragment.IAutoConnect, AsyncExploreController.IAsyncSearchExplore<VenueSearchRoot>
{
    private Button btnOK;
    private Button btnScan;
    private EditText txtMac;
    private LinearLayout ll = null;
    private MessageHandler handler;
    private InputMethodManager imm;
    private Context ctx;
    private DialogFragment diag;
    private TextView lblTap;
    WifiManager wifi;
    ArrayList<String> results;
    private ProgressReceiver progress;
    private Intent autoConnectIntent;
    private AdView adview;
    private ProgressBarWrapper pg;
    private ArrayList<KeygenThread> keygenList;
    private static int mSuccessCounter, mErrorCounter;
    private Tracker tracker;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // supportRequestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.main);
        ctx = MainActivity.this;
        handler = new MessageHandler(this);
        ll = (LinearLayout) findViewById(R.id.keys); // point to dynamic layout
        txtMac = (EditText) findViewById(R.id.txtMac);
        btnScan = (Button) findViewById(R.id.btnScan);
        btnOK = (Button) findViewById(R.id.btnOK);
        lblTap = (TextView) findViewById(R.id.tap_to_copy);
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        progress = new ProgressReceiver(this);
        LocalBroadcastManager.getInstance(ctx)
                .registerReceiver(progress, new IntentFilter(ProgressReceiver.FilterName));
        btnOK.setOnClickListener(this);
        btnScan.setOnClickListener(this);
        autoConnectIntent = new Intent(this, AutoConnectService.class);
        Integrity.checkIntegrity(this);
        pg = new ProgressBarWrapper(this, getWindow().getDecorView());
        keygenList = new ArrayList<>();
        results = new ArrayList<>();
        mSuccessCounter = mErrorCounter = 0;
        ll.removeAllViews();
        prefs.migrate();
        tracker = ((GreekWpaApp) getApplication()).getTracker();
        /*
		 * Locale locale = new Locale("el"); Locale.setDefault(locale);
		 * Configuration config = new Configuration(); config.locale = locale;
		 * getBaseContext().getResources().updateConfiguration(config,
		 * getBaseContext().getResources().getDisplayMetrics());
		 */

        if (!prefs.isPremium().equals(MyBilling.ITEM_PURCHASED))
        {
            InterstitialAd interstitial = new InterstitialAd(this);
            interstitial.setAdUnitId(Integrity.INTERSTITIAL_ID);
            interstitial.setAdListener(new InterstitialListener(interstitial, this));
            interstitial.loadAd(new AdRequest.Builder().build());
            prepareBannerView();

        }
        new MyCheckBilling(this).initializeBilling();
        //new  AsyncExploreController(this).execute(40.6088,22.98034);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        if (!prefs.isPremium().equals(MyBilling.ITEM_PURCHASED))
        {
            prepareBannerView();
        }

    }

    @Override
    protected void onStop()
    {
        GoogleAnalytics.getInstance(ctx).reportActivityStop(this);
        super.onStop();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if (isAutoConnectServiceRunning())
        {
            pg.show();
        }
        GoogleAnalytics.getInstance(ctx).reportActivityStart(this);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (btnOK != null) btnOK.setEnabled(true);
        if (btnScan != null) btnScan.setEnabled(true);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        if (!keygenList.isEmpty())
        {
            for (KeygenThread calc : keygenList)
                calc.setStopRequested(true);
        }
        cancelDialog();
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        disableReceivers();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnScan:
                btnScan.setEnabled(false);
                Intent intent = new Intent(ctx, ScannerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                break;
            case R.id.btnOK:
                String strMac = txtMac.getText().toString();
                strMac = strMac.toUpperCase(Locale.getDefault()).replace(":", "").replace("-", "")
                        .replace(" ", "");
                imm.hideSoftInputFromWindow(txtMac.getWindowToken(), 0); // hide
                // keyboard
                ll.removeAllViews();
                lblTap.setVisibility(View.INVISIBLE);
                keygenList = RouterFactory.getRouter(strMac, ctx, handler, tracker);
                if (keygenList.isEmpty()) return;
                for (KeygenThread calc : keygenList)
                {
                    calc.start();
                }
                if (strMac.length() == 6)
                {
                    diag = ProgressDialogFrag.newInstance(R.string.pleaseWait, getResources()
                            .getString(R.string.dialogLoad));
                    btnOK.setEnabled(false);
                    showDialog(diag);
                }
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        setIntent(intent);
        if (intent.getExtras() != null)
        {
            txtMac.setText(getIntent().getStringExtra("ROUTERBRAND"));
            ll.removeAllViews();
        }
    }

    private void disableReceivers()
    {
        ArrayList<WiFiScanReceiver> scanReceiver = WiFiScanReceiver.getInstances();
        if (scanReceiver != null)
        {
            for (WiFiScanReceiver scan : scanReceiver)
            {
                try
                {
                    unregisterReceiver(scan);
                    scan = null;
                }
                catch (Exception e)
                {
                }
            }
        }
        try
        {
            LocalBroadcastManager.getInstance(ctx).unregisterReceiver(progress);
            progress = null;
        }
        catch (Exception e)
        {
            Log.w("receover", "UNREGISTER PROGRESS DESTROY");
        }
    }

    private void setAutoTextKey()
    {
        results.clear();
        for (KeygenThread calc : keygenList)
        {
            for (String result : calc.getResults())
            {
                results.add(result);
            }
        }
        if (!isFinishing())
        {
            if (!results.isEmpty() && results != null)
            {
                try
                {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.keys, KeyTextFragment
                            .newInstance(results.get(results.size() - 1)), "keys");
                    for (int i = results.size() - 2; i >= 0; i--)
                    {
                        Fragment frag = KeyTextFragment.newInstance(results.get(i));
                        ft.add(R.id.keys, frag, "keys");
                    }
                    ft.commit();
                }
                catch (Exception e)
                {
                    Toast.makeText(ctx, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }

        }

    }

    private void setAutoConnectButton()
    {

        if (!isFinishing())
        {
            boolean shouldCommit = false;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prevFrag = getSupportFragmentManager().findFragmentByTag("autoButton");
            Fragment prevFrag2 = getSupportFragmentManager().findFragmentByTag("progress");
            if (prevFrag != null && ft != null)
            {
                ft.remove(prevFrag);
                shouldCommit = true;
            }
            if (prevFrag2 != null && ft != null)
            {
                ft.remove(prevFrag2);
                shouldCommit = true;
            }

            if (results != null && !results.isEmpty())
            {
                ft.add(R.id.keys, new AutoConnectButtonFragment(), "autoButton");
                shouldCommit = true;
            }
            if (shouldCommit) ft.commit();
        }

    }

    private void doSuccess()
    {
        cancelDialog();
        setAutoTextKey();
        setAutoConnectButton();
        lblTap.setVisibility(View.VISIBLE);
        btnOK.setEnabled(true);
    }

    @Override
    public void doError()
    {
        cancelDialog();
        lblTap.setVisibility(View.INVISIBLE);
        Toast.makeText(ctx, getResources().getString(R.string.toastError), Toast.LENGTH_LONG)
                .show();
        if (!keygenList.isEmpty())
        {
            for (KeygenThread calc : keygenList)
                calc.setStopRequested(true);
        }
        btnOK.setEnabled(true);
    }

    @Override
    public void doSuccessOrError(int code)
    {
        if (code == KeygenThread.RESULTS_READY)
        {
            mSuccessCounter++;
        }
        else if (code == KeygenThread.ERROR_MSG) mErrorCounter++;

        if (mSuccessCounter + mErrorCounter == keygenList.size())
        {
            if (mErrorCounter == keygenList.size())
            {
                doError();
            }
            else
            {
                doSuccess();
            }

            mSuccessCounter = 0;
            mErrorCounter = 0;
        }

    }

    @Override
    public void deactivateProgressBar()
    {
        // setSupportProgressBarVisibility(false);
        pg.hide();
        stopService(autoConnectIntent);
    }

    @Override
    public void activateReceiver()
    {
        autoConnectIntent.putStringArrayListExtra("keys", results);
        autoConnectIntent.putExtra("desiredMac", txtMac.getText().toString());

        if (!isAutoConnectServiceRunning())
        {
            startService(autoConnectIntent);
            // setSupportProgressBarVisibility(true);
            pg.show();
        }
        else
        {
            diag = ServiceAlertDialogFrag
                    .newInstance(R.string.cancelAutoConnect, R.string.stillRunning);
            showDialog(diag);
        }
    }

    private boolean isAutoConnectServiceRunning()
    {
        final ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if ("com.Fisherman.Greekwpa.AutoConnectService".equals(service.service.getClassName()))
            {
                return true;
            }

        }

        return false;
    }

    private void prepareBannerView()
    {
        final LinearLayout llForAds = (LinearLayout) findViewById(R.id.llforads);
        adview = null;
        adview = new AdView(this);
        adview.setAdUnitId(Integrity.BANNER_ID);
        adview.setAdSize(AdSize.SMART_BANNER);
        adview.setAdListener(new AdListener()
        {

            @Override
            public void onAdLoaded()
            {
                super.onAdLoaded();
                llForAds.removeAllViews();
                llForAds.addView(adview);
                adview.refreshDrawableState();
            }

        });
        adview.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onExploreCompleted(VenueSearchRoot result)
    {
        Log.i("Main", result.getVenues().get(0).getName());
    }
}