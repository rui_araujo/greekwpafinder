package com.Fisherman.Greekwpa.models;

import java.util.ArrayList;

public class VenueSearchRoot
{
	private ArrayList<Venue> venues;

	public VenueSearchRoot(){}

	public ArrayList<Venue> getVenues()
	{
		return venues;
	}

	public void setVenues(ArrayList<Venue> venues)
	{
        this.venues = venues;
	}
}