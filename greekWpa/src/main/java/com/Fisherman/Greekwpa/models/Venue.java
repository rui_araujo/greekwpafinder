package com.Fisherman.Greekwpa.models;

import java.util.ArrayList;
import java.util.Comparator;

public class Venue
{
    public static final Comparator<Venue> COMPARE_BY_LOCATION = new Comparator<Venue>()
    {

        @Override
        public int compare(Venue lhs, Venue rhs)
        {
            return lhs.getLocation().getDistance() - rhs.getLocation().getDistance();
        }
    };
    private String id;
    private Location location;
    private String name;
    private ArrayList<Tip> tips;
    private String listedItemId;
    private boolean isFavored;
    private boolean isOnProgress;

    public Venue()
    {
    }

    public String getId()
    {
        return this.id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public ArrayList<Tip> getTips()
    {
        return tips;
    }

    public void setTips(ArrayList<Tip> tips)
    {
        this.tips = tips;
    }

    public String getListedItemId()
    {
        return listedItemId;
    }

    public void setListedItemId(String listedItemId)
    {
        this.listedItemId = listedItemId;
    }

    public boolean isFavored()
    {
        return isFavored;
    }

    public void setFavored(boolean isFavored)
    {
        this.isFavored = isFavored;
    }

    public boolean isOnProgress()
    {
        return isOnProgress;
    }

    public void setOnProgress(boolean isOnProgress)
    {
        this.isOnProgress = isOnProgress;
    }
}
