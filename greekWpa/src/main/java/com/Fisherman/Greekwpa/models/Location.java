
package com.Fisherman.Greekwpa.models;

import java.util.ArrayList;

public class Location
{
 	private String address;
 	private String crossStreet;
	private double lat;
   	private double lng;
   	private int distance;
   	private String postalCode;
   	private String cc;
   	private String city;
 	private String state;
 	private String country;
 	private String neighborhood;
 	private boolean isFuzzed;
 	private ArrayList<String> formattedAddress;
 	
 	public Location()
	{
	}
	public String getAddress(){
		return this.address;
	}
	public void setAddress(String address){
		this.address = address;
	}
 	public String getCc(){
		return this.cc;
	}
	public void setCc(String cc){
		this.cc = cc;
	}
 	public String getCity(){
		return this.city;
	}
	public void setCity(String city){
		this.city = city;
	}
 	public String getCountry(){
		return this.country;
	}
	public void setCountry(String country){
		this.country = country;
	}
 	public String getCrossStreet(){
		return this.crossStreet;
	}
	public void setCrossStreet(String crossStreet){
		this.crossStreet = crossStreet;
	}
 	public int getDistance(){
		return this.distance;
	}
	public void setDistance(int distance){
		this.distance = distance;
	}

 	public double getLat()
	{
		return lat;
	}
	public void setLat(double lat)
	{
		this.lat = lat;
	}
	public double getLng()
	{
		return lng;
	}
	public void setLng(double lng)
	{
		this.lng = lng;
	}
	public String getPostalCode(){
		return this.postalCode;
	}
	public void setPostalCode(String postalCode){
		this.postalCode = postalCode;
	}
 	public String getState(){
		return this.state;
	}
	public void setState(String state){
		this.state = state;
	}
	public ArrayList<String> getFormattedAddress()
	{
		return formattedAddress;
	}
	public void setFormattedAddress(ArrayList<String> formattedAddress)
	{
		this.formattedAddress = formattedAddress;
	}
	public boolean isFuzzed()
	{
		return isFuzzed;
	}
	public void setFuzzed(boolean isFuzzed)
	{
		this.isFuzzed = isFuzzed;
	}
	public String getNeighborhood()
	{
		return neighborhood;
	}
	public void setNeighborhood(String neighborhood)
	{
		this.neighborhood = neighborhood;
	}

}
