package com.Fisherman.Greekwpa;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import com.Fisherman.Greekwpa.Algorithms.KeygenThread;

public class MessageHandler extends Handler {
	public interface IActivityHandler 
	{
		public void doError();
		public void doSuccessOrError(int code);
	}
	private final IActivityHandler activityHandler;
	
	public MessageHandler(Activity main) {
		activityHandler = (IActivityHandler) main;
	}
	
	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		if ( msg.what == KeygenThread.RESULTS_READY)
		{
			activityHandler.doSuccessOrError(KeygenThread.RESULTS_READY);
		}
		if ( msg.what == KeygenThread.ERROR_MSG)
		{
			activityHandler.doSuccessOrError(KeygenThread.ERROR_MSG);
		}
	}

}