package com.Fisherman.Greekwpa;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

public class ProgressBarWrapper
{
	private final boolean mAllowFade;
	final ProgressBar mProgressBar;

	@SuppressLint("InflateParams")
	public ProgressBarWrapper(Context context, final View decorView)
	{
		mAllowFade = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;

		mProgressBar = (ProgressBar) LayoutInflater.from(context).inflate(R.layout.smooth_progressbar, null, false);
		mProgressBar.setVisibility(View.GONE);
		
		 // get the action bar layout
		int actionBarId = context.getResources().getIdentifier("action_bar_container", "id", "android");
        FrameLayout f = (FrameLayout) decorView.findViewById(actionBarId);
        if (f == null) {
            f = (FrameLayout) decorView.findViewById(android.support.v7.appcompat.R.id.action_bar_container);
        }
		// add the view to that layout   		
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.BOTTOM;
		params.bottomMargin = -6 ;
		f.addView(mProgressBar, params);
       
	}


	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void show()
	{
		mProgressBar.setVisibility(View.VISIBLE);
		if (mAllowFade)
		{
			ObjectAnimator.ofFloat(mProgressBar, "alpha", 0f, 1f).start();
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void hide()
	{
		if (mAllowFade)
		{
			ObjectAnimator anim = ObjectAnimator.ofFloat(mProgressBar, "alpha", 1f, 0f);
			anim.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation)
				{
					mProgressBar.setVisibility(View.GONE);
				}
			});
			anim.start();
		}
		else
		{
			mProgressBar.setVisibility(View.GONE);
		}
	}
}
