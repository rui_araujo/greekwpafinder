package com.Fisherman.Greekwpa.billingutil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.util.Log;

import com.Fisherman.Greekwpa.GreekWpaApp;
import com.Fisherman.Greekwpa.MyPreferences;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class MyCheckBilling
{
	private final Tracker t;
	private static final String TAG = "MyCheckBilling";
	private final Activity activity;
	private IabHelper mHelper = null;

    public MyCheckBilling(Activity activity)
	{
		this.activity = activity;
		t = ((GreekWpaApp) activity.getApplication()).getTracker();
	}

	public void initializeBilling()
	{
		// Create the helper, passing it our context and the public key to
		// verify signatures with
		Log.d(TAG, "Creating IAB helper.");
		mHelper = new IabHelper(activity, MyBilling.base64EncodedPublicKey);
		// enable debug logging (for a production application, you should set
		// this to false).
		mHelper.enableDebugLogging(false);

		// Start setup. This is asynchronous and the specified listener
		// will be called once setup completes.
		Log.d(TAG, "Starting setup.");
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result)
			{
				Log.d(TAG, "Setup finished.");
				if (mHelper == null)
					return;

				if (!result.isSuccess())
				{
					t.send(new HitBuilders.EventBuilder(TAG, "billing setup failed" + result).build());
					disposeIabHelper();
					return;
				}

				// IAB is fully set up. Now, let's get an inventory of stuff we
				// own.
				Log.d(TAG, "Setup successful. Querying inventory.");
				mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		});

	}

	// Listener that's called when we finish querying the items and
	// subscriptions we own
	private final IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result, Inventory inventory)
		{
			Log.d(TAG, "Query inventory finished.");

			// Have we been disposed of in the meantime? If so, quit.
			if (mHelper == null)
				return;

			// Is it a failure?
			if (result.isFailure())
			{
				t.send(new HitBuilders.EventBuilder(TAG, "Failed to query inventory " + result).build());
				disposeIabHelper();
				return;
			}

			Log.d(TAG, "Query inventory was successful.");

			Purchase premiumPurchase = inventory.getPurchase(MyBilling.SKU_PREMIUM);
            boolean mIsPremium = (premiumPurchase != null);
			if (mIsPremium)
			{
				new MyPreferences(activity).putPremiumUser();
				disposeIabHelper();
			}
			else
			{
				new MyPreferences(activity).putNormalUser();
				disposeIabHelper();
			}

			Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));
		}
	};

	public boolean handleActivityResult(int requestCode, int resultCode, Intent data)
	{
        return mHelper != null && mHelper.handleActivityResult(requestCode, resultCode, data);
    }

	protected void alert(String message)
	{
		AlertDialog.Builder bld = new AlertDialog.Builder(activity);
		bld.setMessage(message);
		bld.setNeutralButton("OK", null);
		bld.create().show();
	}

	public void disposeIabHelper()
	{
		if (mHelper != null)
		{
			mHelper.dispose();
			mHelper = null;
		}
	}
}
