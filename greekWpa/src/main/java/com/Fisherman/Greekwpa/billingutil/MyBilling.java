package com.Fisherman.Greekwpa.billingutil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.Fisherman.Greekwpa.GreekWpaApp;
import com.Fisherman.Greekwpa.MyPreferences;
import com.Fisherman.Greekwpa.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class MyBilling
{
	private final Tracker t;
	private static final String TAG = "MyBilling";

	private final Activity activity;
	private IabHelper mHelper = null;
	private static final int RC_REQUEST = 10001;
	private static boolean mIsPremium = false;
	public static final String SKU_PREMIUM = "com.fisherman.greekwpa.permanent_ad_removal";
	public static final String ITEM_PURCHASED = "item.fucking.purchased.man.yeah!";
	public static final String ITEM_NOT_PURCHASED = "nah.item.not.purchased.man";
	public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAku9wXvrxiAw3/"
			+ "rsGOGrp6Q0JC2zCiJ3TNIAm7qGRSgxNCyiW/TV9AkYblajqWmUWjjAKyKf1mEd7AOBlY5kngdIDp5pjutr6ZvL" 
			+ "kzpz1djBtlA+Yxbm7PeXjZbw7ofveL9eBiqhduI76N6zmEUTHiJQSD1rI7g9+MVHADpOB/89AzTfikknTV"
			+ "GjyY1fWzrzRI+jDo/6x3U1bDAWZthW21JJmZAEws/DoxPymdCVmDielalgUUkUE0h4Ku41vb1eCozW+ETNu" 
			+ "P5HHxSEG+BW1jxS7jCqu3h2E19a/Gx5hYMb20m+01ALPLi3dJfxixMOUf1tkbitnr2o6BH5m8jJIgwIDAQAB";

	public MyBilling(Activity activity)
	{
		this.activity = activity;
		t = ((GreekWpaApp) activity.getApplication()).getTracker();
	}

	public void initializeBilling()
	{
		// Create the helper, passing it our context and the public key to
		// verify signatures with
		Log.d(TAG, "Creating IAB helper.");
		mHelper = new IabHelper(activity, base64EncodedPublicKey);
		// enable debug logging (for a production application, you should set
		// this to false).
		mHelper.enableDebugLogging(false);

		// Start setup. This is asynchronous and the specified listener
		// will be called once setup completes.
		Log.d(TAG, "Starting setup.");
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result)
			{
				Log.d(TAG, "Setup finished.");
				if (mHelper == null)
					return;

				if (!result.isSuccess())
				{
					t.send(new HitBuilders.EventBuilder(TAG, "billing setup failed " + result).build());
					alert(activity.getResources().getString(R.string.purchase_setup_error) + result);
					disposeIabHelper();
					return;
				}

				// IAB is fully set up. Now, let's get an inventory of stuff we
				// own.
				Log.d(TAG, "Setup successful. Querying inventory.");
				try 
				{
					mHelper.queryInventoryAsync(mGotInventoryListener);
				}
				catch (IllegalStateException e)
				{
                }
				
			}
		});
	}

	// Listener that's called when we finish querying the items and
	// subscriptions we own
	private final IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result, Inventory inventory)
		{
			Log.d(TAG, "Query inventory finished.");

			// Have we been disposed of in the meantime? If so, quit.
			if (mHelper == null)
				return;

			// Is it a failure?
			if (result.isFailure())
			{
				alert("Failed to query inventory: " + result);
				t.send(new HitBuilders.EventBuilder(TAG, "Failed to query inventory " + result).build());
				disposeIabHelper();
				return;
			}

			Log.d(TAG, "Query inventory was successful.");

			Purchase premiumPurchase = inventory.getPurchase(SKU_PREMIUM);
			mIsPremium = (premiumPurchase != null);
			if (mIsPremium)
			{
				new MyPreferences(activity).putPremiumUser();
				Toast.makeText(activity, R.string.premium_check_ok, Toast.LENGTH_LONG).show();
				disposeIabHelper();
			}
			else
			{
				new MyPreferences(activity).putNormalUser();
				purchasePremium();
			}

			Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));
		}
	};

	private void purchasePremium()
	{

		mHelper.launchPurchaseFlow(activity, SKU_PREMIUM, RC_REQUEST, mPurchaseFinishedListener);
	}

	private final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase purchase)
		{
			if (mHelper == null)
				return;

			if (result.isFailure())
			{
				alert(activity.getResources().getString(R.string.purchase_error) + result);
				t.send(new HitBuilders.EventBuilder(TAG, "purchase failed " + result).build());
				disposeIabHelper();
				return;
			}
			Log.d(TAG, "Purchase successful.");
			if (purchase.getSku().equals(SKU_PREMIUM))
			{
				// bought the premium upgrade!
				Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
				new MyPreferences(activity).putPremiumUser();
				Toast.makeText(activity, R.string.purchase_thanks, Toast.LENGTH_LONG).show();
				t.send(new HitBuilders.EventBuilder(TAG, "Purchase SUCCESS!").build());
				mIsPremium = true;
				disposeIabHelper();
			}
		}
	};

	public boolean handleActivityResult(int requestCode, int resultCode, Intent data)
	{
        return mHelper != null && mHelper.handleActivityResult(requestCode, resultCode, data);
    }

	protected void alert(String message)
	{
		AlertDialog.Builder bld = new AlertDialog.Builder(activity);
		bld.setMessage(message);
		bld.setNeutralButton("OK", null);
		bld.create().show();
	}

	public void disposeIabHelper()
	{
		if (mHelper != null)
		{
			mHelper.dispose();
			mHelper = null;
		}
	}
}
