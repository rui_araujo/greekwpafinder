package com.Fisherman.Greekwpa.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ProgressReceiver extends BroadcastReceiver
{
	public interface IProgressBar
	{
		public void deactivateProgressBar();
	}
	private final IProgressBar pg;
	public static final String FilterName = "Thanos.Progressbar";
	public ProgressReceiver(IProgressBar pg)
	{
		this.pg = pg;
	}

	@Override
	public void onReceive(Context context, Intent intent)
	{
		pg.deactivateProgressBar();
		Log.d("PROGRESSRECEIVER", "YAY I RECEIVED INTENT");
	}

}
