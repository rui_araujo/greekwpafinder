package com.Fisherman.Greekwpa.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

public class WiFiScanReceiver extends BroadcastReceiver
{
	public interface IGetScannedResults
	{
		public void getScanResults();
	}

	private final IGetScannedResults scanner;
	private static ArrayList<WiFiScanReceiver> instances;

	private WiFiScanReceiver(IGetScannedResults scanner)
	{
		this.scanner = scanner;
	}

	public static WiFiScanReceiver newInstance(IGetScannedResults scanner)
	{
		WiFiScanReceiver scanR = new WiFiScanReceiver(scanner);
		if (instances == null)
			instances = new ArrayList<>();
		instances.add(scanR);

		return scanR;
	}

	public static ArrayList<WiFiScanReceiver> getInstances()
	{
		return instances;
	}

	@Override
	public void onReceive(Context c, Intent intent)
	{
		scanner.getScanResults();

	}

}
