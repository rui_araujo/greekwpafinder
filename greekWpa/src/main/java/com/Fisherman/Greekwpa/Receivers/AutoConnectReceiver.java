package com.Fisherman.Greekwpa.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.ArrayList;

public class AutoConnectReceiver extends BroadcastReceiver
{
	public interface IAutoConnectListener
	{
		public void onSuccessfulConnection();
		public void onFailedConnection();
	}
	private static ArrayList<AutoConnectReceiver>instances;
	private final IAutoConnectListener connection;
	private int attempts = 0;
	
	private AutoConnectReceiver(IAutoConnectListener connection)
	{
		this.connection = connection;
	}

	public static AutoConnectReceiver newInstance(IAutoConnectListener connection)
	{
		AutoConnectReceiver autoConnectR = new AutoConnectReceiver(connection);
		if (instances == null)
			instances = new ArrayList<>();
		instances.add(autoConnectR);
		return autoConnectR;
	}
	public static ArrayList<AutoConnectReceiver> getInstances()
	{
		return instances;
	}
	@Override
	public void onReceive(Context context, Intent intent)
	{
		SupplicantState state = intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
		if (state == null)
			return;
		Log.d(this.getClass().getSimpleName(), "state: " + state.name());
		if (state.equals(SupplicantState.COMPLETED))
		{
			connection.onSuccessfulConnection();
			return;
		}
		if (state.equals(SupplicantState.DISCONNECTED))
		{ 
			
			Log.d("AutoReceiver", "ATTEMPTS " + attempts);
			if (attempts >= 1)
			{
				connection.onFailedConnection();
				attempts = 0;
			}
			attempts++;
        }
		/*if (state.equals(SupplicantState.INACTIVE))
		{
			connection.onFailedConnection();
			return;
		} */
	}

}
