package com.Fisherman.Greekwpa;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import com.Fisherman.Greekwpa.Algorithms.CYTA;
import com.Fisherman.Greekwpa.Algorithms.CytaZTE;
import com.Fisherman.Greekwpa.Algorithms.Dlink;
import com.Fisherman.Greekwpa.Algorithms.Huawei1;
import com.Fisherman.Greekwpa.Algorithms.KeygenThread;
import com.Fisherman.Greekwpa.Algorithms.NetFaster;
import com.Fisherman.Greekwpa.Algorithms.OteBaudtec;
import com.Fisherman.Greekwpa.Algorithms.OteZte;
import com.Fisherman.Greekwpa.Algorithms.Thomson;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

public class RouterFactory
{
	private static final ArrayList<KeygenThread> keygenList = new ArrayList<>();
	//private static final Tracker t = ((GreekWpaApp) getApplication()).getTracker();
	public static ArrayList<KeygenThread> getRouter(String strMac, Context ctx, Handler handler, Tracker t)
	{
		keygenList.clear();
		if (strMac.startsWith("000559"))
		{
			keygenList.add(new NetFaster(strMac, ctx, handler));
			t.send(new HitBuilders.EventBuilder("Algorithms", "NetFaster").build());
		}
		if (strMac.startsWith("CC1AFA") || strMac.startsWith("146080") || strMac.startsWith("DC028E") || strMac.startsWith("CC7B35")
				|| strMac.startsWith("208986") || strMac.startsWith("2C957F") || strMac.startsWith("F8DFA8") || strMac.startsWith("EC8A4C"))
		{
			keygenList.add(new CytaZTE(strMac, ctx, handler));
			t.send(new HitBuilders.EventBuilder("Algorithms", "CytaZTE").build());
		}
		if (strMac.startsWith("F4C714") || strMac.startsWith("6416F0") || strMac.startsWith("5C4CA9") || strMac.startsWith("54A51B") || strMac.startsWith("548998")
				|| strMac.startsWith("4C5499") || strMac.startsWith("4C1FCC") || strMac.startsWith("404D8E") || strMac.startsWith("308730") || strMac.startsWith("286ED4")
				|| strMac.startsWith("285FDB") || strMac.startsWith("24DBAC") || strMac.startsWith("20F3A3") || strMac.startsWith("202BC1") || strMac.startsWith("1C1D67")
				|| strMac.startsWith("10C61F") || strMac.startsWith("0C37DC") || strMac.startsWith("0819A6") || strMac.startsWith("04C06F") || strMac.startsWith("00259E")
				|| strMac.startsWith("002568") || strMac.startsWith("0022A1") || strMac.startsWith("001E10") || strMac.startsWith("001915") || strMac.startsWith("001882")
				|| strMac.startsWith("0011F5") || strMac.startsWith("000FF2") || strMac.startsWith("00E0FC") || strMac.startsWith("781DBA") || strMac.startsWith("84A8E4")
				|| strMac.startsWith("CC96A0"))
		{
			keygenList.add(new Huawei1(strMac, ctx, handler));
			t.send(new HitBuilders.EventBuilder("Algorithms", "Huawei1").build());
		}
		if ( strMac.startsWith("48282F") || strMac.startsWith("B075D5") || strMac.startsWith("C87B5B") || strMac.startsWith("FCC897") || strMac.startsWith("681AB2") || strMac.startsWith("384608")
				|| strMac.startsWith("4C099B") || strMac.startsWith("4C09B4") || strMac.startsWith("8CE081") || strMac.startsWith("DC028E") || strMac.startsWith("2C26C5")
				|| strMac.startsWith("FCC897") || strMac.startsWith("CC1AFA") || strMac.startsWith("A0EC80") || strMac.startsWith("5422F8") || strMac.startsWith("146080"))
		{
			keygenList.add(new OteZte(strMac, ctx, handler));
			t.send(new HitBuilders.EventBuilder("Algorithms", "OteZte").build());
		}

		if (strMac.startsWith("001CA2") || strMac.startsWith("0017C2") || strMac.startsWith("00193E") || strMac.startsWith("001CA2") || strMac.startsWith("00238E")
				|| strMac.startsWith("002553") || strMac.startsWith("38229D") || strMac.startsWith("6487D7") || strMac.startsWith("DC0B1A"))
		{
			keygenList.add(new CYTA(strMac, ctx, handler));
			t.send(new HitBuilders.EventBuilder("Algorithms", "CytaPirelli").build());
		}
		if (strMac.length() == 6)
		{
			keygenList.add(new Thomson(strMac, ctx, handler));
			t.send(new HitBuilders.EventBuilder("Algorithms", "Thomson").build());
		}
		if (strMac.startsWith("000D88") || strMac.startsWith("000F3D") || strMac.startsWith("001195") || strMac.startsWith("001346") || strMac.startsWith("0015E9")
				|| strMac.startsWith("00179A") || strMac.startsWith("00195B") || strMac.startsWith("001B11") || strMac.startsWith("001CF0") || strMac.startsWith("001E58")
				|| strMac.startsWith("002191") || strMac.startsWith("0022B0") || strMac.startsWith("002401") || strMac.startsWith("00265A") || strMac.startsWith("340804")
				|| strMac.startsWith("5CD998") || strMac.startsWith("F07D68") || strMac.startsWith("B8A386"))
		// H teleutaia mac einai apo D-Link International me ssid DLink de kserw
		// an doulevei
		{
			keygenList.add(new Dlink(strMac, ctx, handler));
			t.send(new HitBuilders.EventBuilder("Algorithms", "Dlink").build());
		}
		if (strMac.startsWith("001333"))
		{
			keygenList.add(new OteBaudtec(strMac, ctx, handler));
			t.send(new HitBuilders.EventBuilder("Algorithms", "OteBaudtec").build());
		}
		if (keygenList.isEmpty())
		{
			Toast.makeText(ctx, R.string.notSupported, Toast.LENGTH_SHORT).show();
			t.send(new HitBuilders.EventBuilder("Algorithms", "Not Supported").build());
		}

		return keygenList;

	}

}
