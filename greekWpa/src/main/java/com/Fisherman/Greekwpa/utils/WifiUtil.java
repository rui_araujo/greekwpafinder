package com.Fisherman.Greekwpa.utils;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.widget.Toast;

import com.Fisherman.Greekwpa.R;

public class WifiUtil
{
	public static boolean enableWifiAndScan(Context ctx, WifiManager wifi)
	{

		if (wifi.isWifiEnabled())
		{
			handleToasts(ctx, R.string.toastScan);
			if (wifi.startScan())
				return true;
		}
		else if (wifi.setWifiEnabled(true))
		{
			handleToasts(ctx, R.string.toastEnableAndScan);
			if (wifi.startScan())
				return true;
		}
		else
		{
			handleToasts(ctx, R.string.toastCantEnable);
		}
		return false;

	}
	private static void handleToasts(final Context ctx,final int message)
	{
		Runnable handleToast = new Runnable() {
			
			@Override
			public void run()
			{
				Toast.makeText(ctx, ctx.getText(message), Toast.LENGTH_LONG).show();
			}
		};
		new Handler().post(handleToast); 
	}

}
