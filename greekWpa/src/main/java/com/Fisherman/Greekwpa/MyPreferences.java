package com.Fisherman.Greekwpa;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings.Secure;

import com.Fisherman.Greekwpa.billingutil.AESObfuscator;
import com.Fisherman.Greekwpa.billingutil.MyBilling;
import com.Fisherman.Greekwpa.billingutil.Obfuscator;
import com.Fisherman.Greekwpa.billingutil.PreferenceObfuscator;

public class MyPreferences
{
	private static final String PREFERENCES_VERSION_KEY = "prefs_version";
	private static final String DIALOG_CONTRIBUTE_KEY = "dialog_contribute_blocked";
	public static final int PREFERENCES_VERSION_NUMBER = 5;
	public static final String IS_PREMIUM = "is_premium_user";
	private final SharedPreferences mPrefs;
	private SharedPreferences.Editor mEditor;
	private final String deviceId;
	private final Context ctx;

	public MyPreferences(Context ctx)
	{
		this.ctx = ctx;
		deviceId = Secure.getString(ctx.getContentResolver(), Secure.ANDROID_ID);
		this.mPrefs = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);

	}

	public boolean isDialogContributeBlocked()
	{
        return mPrefs != null && mPrefs.getBoolean(DIALOG_CONTRIBUTE_KEY, false);
    }

	public void putBlockDialogContribute()
	{
		if (mPrefs != null)
		{
			mEditor = mPrefs.edit();
			mEditor.putBoolean(DIALOG_CONTRIBUTE_KEY, true);
			mEditor.apply();
		}

	}

	public void migrate()
	{
		if (mPrefs != null)
		{
			mEditor = mPrefs.edit();
			int version = mPrefs.getInt(PREFERENCES_VERSION_KEY, 1);
			if (version < PREFERENCES_VERSION_NUMBER)
			{
				mEditor.clear();
				mEditor.putInt(PREFERENCES_VERSION_KEY, PREFERENCES_VERSION_NUMBER);
				mEditor.apply();
			}
		}
	}

	public String isPremium()
	{
		Obfuscator obf = new AESObfuscator(Integrity.alataki, ctx.getPackageName(), deviceId);
		PreferenceObfuscator prefObf = new PreferenceObfuscator(mPrefs, obf);
		String premiumCode = prefObf.getString(IS_PREMIUM, MyBilling.ITEM_NOT_PURCHASED);
		return premiumCode;

	}

	public void putPremiumUser()
	{
		Obfuscator obf = new AESObfuscator(Integrity.alataki, ctx.getPackageName(), deviceId);
		PreferenceObfuscator prefObf = new PreferenceObfuscator(mPrefs, obf);
		prefObf.putString(IS_PREMIUM, MyBilling.ITEM_PURCHASED);
		prefObf.apply();
	}

	public void putNormalUser()
	{
		Obfuscator obf = new AESObfuscator(Integrity.alataki, ctx.getPackageName(), deviceId);
		PreferenceObfuscator prefObf = new PreferenceObfuscator(mPrefs, obf);
		prefObf.putString(IS_PREMIUM, MyBilling.ITEM_NOT_PURCHASED);
		prefObf.apply();
	}

}
