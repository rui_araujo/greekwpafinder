package com.Fisherman.Greekwpa;

import android.annotation.SuppressLint;
import android.net.wifi.ScanResult;

import java.util.List;
import java.util.Locale;

public class RouterFilter {

    public static enum TYPE {
		THOMSON,CYTA,NETFASTER,
		OTEZTE,HUAWEI1,DLINK,
		NEWTHOMSON,BAUDTEC,CYTAZTE,OTHER
	}

    @SuppressLint("DefaultLocale")
	public static TYPE compatibleRouter(List<ScanResult> scanResults, int pos)
	{
        String bssid = scanResults.get(pos).BSSID.replace(":", "").toUpperCase(Locale.getDefault());
        String ssid = scanResults.get(pos).SSID;
        TYPE type;
        if ( 	( ssid.startsWith("Thomson") && ssid.length() == 13 )    ||
			     ( ssid.startsWith("SpeedTouch") && ssid.length() == 16 ) ||
			     ( ssid.startsWith("O2Wireless") && ssid.length() == 16 ) ||
			     ( ssid.startsWith("Orange-") && ssid.length() == 13 ) || 
			     ( ssid.startsWith("INFINITUM") && ssid.length() == 15 )  ||
			     ( ssid.startsWith("BigPond") && ssid.length() == 13 )  ||
			     ( ssid.startsWith("Otenet") && ssid.length() == 12 ) ||
			     ( ssid.startsWith("Bbox-") && ssid.length() == 11 ) ||
			     ( ssid.startsWith("DMAX") && ssid.length() == 10 )  || 
			     ( ssid.startsWith("privat") && ssid.length() == 12 ) ||
			     ( ssid.startsWith("TN_private_") && ssid.length() == 17 ) || 
			     ( ssid.matches("CYTA[a-zA-z0-9]{6}") && !ssid.matches("CYTA[0-9]{6}") ) ||
			     ( ssid.startsWith("Blink") && ssid.length() == 11 ))
		{
			if (ssid.substring(ssid.length() - 6).equals(bssid.substring(6)))
			{
				type = TYPE.NEWTHOMSON;
				return type;		
			}	
			type = TYPE.THOMSON;
			return type;		
		}
		if (bssid.startsWith("000559") && (ssid.contains("NetFasteR") || ssid.contains("hol")) )
		{
			 type = TYPE.NETFASTER;
			 return type;
		}
		if (ssid.startsWith("CYTA") && ( bssid.startsWith("CC1AFA") || bssid.startsWith("146080") 
				|| bssid.startsWith("DC028E") || bssid.startsWith("CC7B35") || bssid
                .startsWith("208986")
				|| bssid.startsWith("2C957F") || bssid.startsWith("F8DFA8") || bssid
                .startsWith("EC8A4C")) )
		{
			type = TYPE.CYTAZTE;
			return type;
		}
		
		if ( 	(ssid.startsWith("conn-x") || ssid.matches("OTE[0-9a-fA-F]{6}")) &&
				(bssid.startsWith("B075D5")  || bssid.startsWith("C87B5B") || 
				  bssid.startsWith("FCC897") || bssid.startsWith("681AB2") || 
				  bssid.startsWith("384608") || bssid.startsWith("4C099B") ||
				  bssid.startsWith("4C09B4") || bssid.startsWith("8CE081") || 
				  bssid.startsWith("DC028E") || bssid.startsWith("2C26C5") ||
				  bssid.startsWith("FCC897") || bssid.startsWith("CC1AFA") ||
				  bssid.startsWith("A0EC80") || bssid.startsWith("5422F8") ||
				  bssid.startsWith("146080") || bssid.startsWith("48282F")) )
		{
			type = TYPE.OTEZTE;
			return type;
		}

		if (	bssid.startsWith("F4C714") || bssid.startsWith("6416F0") || 
				bssid.startsWith("5C4CA9") || bssid.startsWith("54A51B") || 
				bssid.startsWith("548998") || bssid.startsWith("4C5499") || 
				bssid.startsWith("4C1FCC") || bssid.startsWith("404D8E") || 
				bssid.startsWith("308730") || bssid.startsWith("286ED4") || 
				bssid.startsWith("285FDB") || bssid.startsWith("24DBAC") || 
				bssid.startsWith("20F3A3") || bssid.startsWith("202BC1") || 
				bssid.startsWith("1C1D67") || bssid.startsWith("10C61F") || 
				bssid.startsWith("0C37DC") || bssid.startsWith("0819A6") || 
				bssid.startsWith("04C06F") || bssid.startsWith("00259E") || 
				bssid.startsWith("002568") || bssid.startsWith("0022A1") || 
				bssid.startsWith("001E10") || bssid.startsWith("001915") || 
				bssid.startsWith("001882") || bssid.startsWith("0011F5") || 
				bssid.startsWith("000FF2") || bssid.startsWith("00E0FC") || 
				bssid.startsWith("781DBA") || bssid.startsWith("84A8E4") || 
				bssid.startsWith("CC96A0") )
		{
			type = TYPE.HUAWEI1;
			return type;
			
		}
		if (ssid.startsWith("CYTA") && 
				  (bssid.startsWith("001CA2") || bssid.startsWith("0017C2") || bssid
                          .startsWith("00193E") ||
				   bssid.startsWith("001CA2") || bssid.startsWith("00238E") || bssid
                          .startsWith("002553") ||
				   bssid.startsWith("38229D") || bssid.startsWith("6487D7") || bssid
                          .startsWith("DC0B1A")) )
		{
			type = TYPE.CYTA;
			return type;
		}
		
		if (ssid.matches("DLink-[0-9a-fA-F]{6}"))
		{
			type = TYPE.DLINK;
			return type;
		}
		if ( ssid.matches("OTE[0-9a-fA-F]{4}") && bssid.startsWith("001333") )
		{
			type = TYPE.BAUDTEC;
			return type;
		}
		type = TYPE.OTHER;
		return type;
		
		}
}
