package com.Fisherman.Greekwpa.Algorithms;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;

public class OteBaudtec extends KeygenThread
{

	public OteBaudtec(String mac, Context ctx, Handler handler)
	{
		super(mac, ctx, handler);
	}

	@Override
	public void run()
	{
		super.run();
		getKey();
	}
	@SuppressLint("DefaultLocale")
	public void getKey() {
		if (mac.length() != 12 )
		{
			//lstResults.add(ctx.getResources().getString(R.string.wrongMac));
			handler.sendEmptyMessage(ERROR_MSG);
			return ;
		}	
		lstResults.add("0"+mac.toLowerCase());
		handler.sendEmptyMessage(RESULTS_READY);
	}

}
