package com.Fisherman.Greekwpa.Algorithms;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;

public class OteZte extends KeygenThread {

	public OteZte(String mac, Context ctx, Handler handler) {
		super(mac, ctx, handler);
	}

	
	@Override
	public void run() {
		getKey();
	}


	@SuppressLint("DefaultLocale")
	public void getKey() {
		if (mac.length() != 12 )
		{
			//lstResults.add(ctx.getResources().getString(R.string.wrongMac));
			handler.sendEmptyMessage(ERROR_MSG);
			return ;
		}
		
		lstResults.add(mac.toLowerCase());
		handler.sendEmptyMessage(RESULTS_READY);
	}

}
