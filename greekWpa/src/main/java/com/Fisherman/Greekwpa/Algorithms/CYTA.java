package com.Fisherman.Greekwpa.Algorithms;

import android.content.Context;
import android.os.Handler;

import com.Fisherman.Greekwpa.R;

import java.util.ArrayList;

public class CYTA extends KeygenThread {

	
	public CYTA(String mac,Context ctx, Handler handler)
	{
		super(mac,ctx,handler);	
	}
	

	@Override
	public void run() {
		//super.run();
		getKey();
	}


	
	public void getKey()
	{
		long key=0;
		int zeros;
		String result = "";
		ArrayList<String> strLine = readAssetsTextFile("cyta_bases.lst"); // readRawTextFile(R.raw.cyta_bases);
	

		for (String lines : strLine )
		{
			String [] strSplit = lines.split(" ");
			
			if (mac.length() != 12 )
			{
				lstResults.add(ctx.getResources().getString(R.string.wrongMac));
				handler.sendEmptyMessage(ERROR_MSG);
				return ;
			}
		
			if ( mac.substring(0,8).equalsIgnoreCase(strSplit[0]) )
			{
				long basi = HexToDec(strSplit[2]);
				int div = Integer.parseInt(strSplit[3]);
				long diff = HexToDec(mac.substring(6)) - basi;
				//System.out.println(diff+ " "+ diff%div);
				if ( (diff >= 0) && (diff <= (9999999)) && (diff % div == 0) )
				{
					key = diff / div;
					zeros = Long.toString(key).length();
					zeros = 7-zeros;
					result = Long.toString(key);
					for(int j=1; j<=zeros; j++)
						result = "0"+result;
					result = strSplit[1]+result;
					lstResults.add(result);
				}
			}
		}
		if (lstResults.isEmpty())
		{
			lstResults.add(ctx.getResources().getString(R.string.noKeysFound));
			handler.sendEmptyMessage(ERROR_MSG);
			return ;
		}
			handler.sendEmptyMessage(RESULTS_READY);
		
	}
	
	
}
