package com.Fisherman.Greekwpa.Algorithms;

import android.content.Context;
import android.os.Handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Thomson extends KeygenThread
{

	private final static int NUMBER_CONST = 46657;
	final private List<ThomsonCalcThread> tasks;

	public Thomson(String mac, Context ctx, Handler handler)
	{
		super(mac, ctx, handler);
		tasks = new ArrayList<>();
	}

	static
	{
		System.loadLibrary("thomson");
	}

	@Override
	public synchronized void setStopRequested(boolean stopRequested)
	{
		super.setStopRequested(stopRequested);
		for (ThomsonCalcThread t : tasks)
			t.stopRequested = true;
	}

	public native String[] thomson(byte[] essid, int start, int end);

	@Override
	public void run()
	{
		getKey();
	}

	public void getKey()
	{

		byte[] ssid = hexStringToByteArray(mac);

		if (this.mac.length() < 6)
		{
			handler.sendEmptyMessage(ERROR_MSG);
			return;
		}

		if (isStopRequested())
			return;

		int cores = Runtime.getRuntime().availableProcessors();
		//System.out.println("CORES " + cores);
		if (cores <= 0)
			cores = 1;
		int work = NUMBER_CONST / cores;
		int beggining = 0;

		for (int i = 1; i < cores; ++i)
		{
			tasks.add(new ThomsonCalcThread(this, ssid, beggining, beggining + work));
			tasks.get(tasks.size() - 1).start();
			beggining += work;
		}
		tasks.add(new ThomsonCalcThread(this, ssid, beggining, NUMBER_CONST));
		tasks.get(tasks.size() - 1).start();
		for (ThomsonCalcThread t : tasks)
		{
			try
			{
				t.join();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
				continue;
			}
			final String[] results = t.results;
			if (t.error)
				handler.sendEmptyMessage(ERROR_MSG);
			if (isStopRequested())
				return;
			lstResults.addAll(Arrays.asList(results));
		}

		if (lstResults.size() == 0)
		{
			handler.sendEmptyMessage(ERROR_MSG);
			return;
		}
		handler.sendEmptyMessage(RESULTS_READY);

    }

	private static class ThomsonCalcThread extends Thread
	{
		private final Thomson keygen;
		private final byte[] routerESSID;
		private final int begin;
		private final int end;
		private boolean error = false;
		private String[] results;

		@SuppressWarnings("unused")
		private boolean stopRequested = false;

		static
		{
			System.loadLibrary("thomson");
		}

		public ThomsonCalcThread(Thomson keygen, byte[] routerESSID, int begin, int end)
		{
			this.keygen = keygen;
			this.routerESSID = routerESSID;
			this.begin = begin;
			this.end = end;
		}

		@Override
		public void run()
		{
			try
			{
				results = keygen.thomson(routerESSID, begin, end);
			}
			catch (Exception e)
			{
				error = true;
			}
		}
	}

	public static byte[] hexStringToByteArray(String s)
	{
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2)
		{
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

}
