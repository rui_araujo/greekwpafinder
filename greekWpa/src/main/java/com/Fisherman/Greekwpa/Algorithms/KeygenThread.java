package com.Fisherman.Greekwpa.Algorithms;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

public abstract class KeygenThread extends Thread
{

	protected final Context ctx;
	protected final ArrayList<String> lstResults;
	protected boolean stopRequested = false;
	protected String mac;
	protected final Handler handler;
	public static final int RESULTS_READY = 1000;
	public static final int ERROR_MSG = 1001;

	public KeygenThread(String mac, Context ctx, Handler handler)
	{

		this.mac = mac.toUpperCase(Locale.getDefault()).replace(":", "").replace("-", "").replace(" ", "");

		this.ctx = ctx;
		this.lstResults = new ArrayList<>();
		this.handler = handler;
	}

	public ArrayList<String> getResults()
	{
		return lstResults;
	}

	public boolean isStopRequested()
	{
		return stopRequested;
	}

	public void setStopRequested(boolean stopRequested)
	{
		this.stopRequested = stopRequested;
	}

	public long HexToDec(String hex)
	{
		long dec = 0;
		try
		{
			dec = Long.parseLong(hex, 16);
		}
		catch (NumberFormatException e)
		{
			return -1;

		}

		return dec;
	}

	/*
	 * public ArrayList<String> readRawTextFile(int ResId) {
	 * 
	 * ArrayList<String> arrStr = new ArrayList<String>(); InputStream is =
	 * ctx.getResources().openRawResource(ResId); InputStreamReader isr = new
	 * InputStreamReader(is); BufferedReader br = new BufferedReader(isr); //
	 * StringBuffer sb = new StringBuffer(); String line = null; try {
	 * 
	 * while ((line = br.readLine()) != null) { // sb.append(line+"\n");
	 * arrStr.add(line); }
	 * 
	 * } catch (IOException e) {
	 * e.printStackTrace(); } finally { try { br.close(); isr.close();
	 * is.close(); } catch (IOException e) {
	 * e.printStackTrace(); }
	 * 
	 * }
	 * 
	 * return arrStr;
	 * 
	 * }
	 */

	public ArrayList<String> readAssetsTextFile(String fileName)
	{
		ArrayList<String> arrStr = new ArrayList<>();
		AssetManager assetManager = ctx.getAssets();
		try
		{
			
			InputStream is = fileName.equals("cyta_bases.lst") ? assetManager.open("cyta_bases.lst") : 
				fileName.equals("cyta_zte_bases.lst") ? assetManager.open("cyta_zte_bases.lst")
					: assetManager.open("netfaster_bases.lst");
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			try
			{
				while ((line = br.readLine()) != null)
				{
					// sb.append(line+"\n");
					arrStr.add(line);
				}
			}
			finally
			{
				br.close();
				isr.close();
				is.close();
			}

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return arrStr;

	}

}
