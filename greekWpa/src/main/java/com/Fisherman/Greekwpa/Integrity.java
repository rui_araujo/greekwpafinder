package com.Fisherman.Greekwpa;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import dexguard.util.CertificateChecker;
import dexguard.util.TamperDetector;

public class Integrity
{
    public static final byte[] alataki = new byte[] { -45, 35, 30, -128, 127, -89, 87, -60, 54, 86, -96, -55, 57, -110, -96, -114, -11, 22, -65, 100 };
    public static final String INTERSTITIAL_ID = "ca-app-pub-4071471797950020/6449561593";
    public static final String BANNER_ID = "ca-app-pub-4071471797950020/7890136392";
    private static final int OK = 3254;
	
	public static void checkIntegrity(Activity activity)
	{
		new AsyncCheckIntegrity().execute(activity);
	}

	private static class AsyncCheckIntegrity extends AsyncTask<Activity, Void, Boolean>
	{
		private Activity activity;

		@Override
		protected Boolean doInBackground(Activity... params)
		{
			this.activity = params[0];
			int apkChanged = TamperDetector.checkApk(activity,OK);
			int certificateChanged = CertificateChecker.checkCertificate(activity,OK);
			return (apkChanged != OK || certificateChanged != OK);
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			super.onPostExecute(result);
			if (result)
			{
				Toast.makeText(activity, "YOU HAVE BEEN A NAUGHTY BOY", Toast.LENGTH_LONG).show();
				activity.finish();
			}
		}

	}
}
