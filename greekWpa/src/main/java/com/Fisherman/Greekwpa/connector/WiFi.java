package com.Fisherman.Greekwpa.connector;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.List;

public class WiFi
{

    public static final String TAG = "WiFi Connecter";
    private static final int MAX_PRIORITY = 99999;

    public static int changePasswordAndConnect(final WifiManager wifiMgr, final ScanResult scanResult, final String newPassword)
    {
        final String security = ConnectorUtils.getSecurity(scanResult);
        WifiConfiguration config = ConnectorUtils.getWifiConfiguration(wifiMgr, scanResult, security);
        if (config == null)
        {
            return -1;
        }
        ConnectorUtils.setupSecurity(config, ConnectorUtils.getSecurity(config), newPassword);
        final int networkId = wifiMgr.updateNetwork(config);
        if (networkId == -1)
        {
            // Update failed.
            return -1;
        }

        return connectToConfiguredNetwork(wifiMgr, config, true);
    }

    public static void cleanPreviousConfiguration(final WifiManager wifiMgr, final ScanResult hotspot)
    {
        WifiConfiguration config;

        config = ConnectorUtils.getWifiConfiguration(wifiMgr, hotspot, null);
        if (config != null) wifiMgr.removeNetwork(config.networkId);
    }

    public static int connectToNewNetwork(final WifiManager wifiMgr, final ScanResult scanResult, final String password)
    {

        final String security = ConnectorUtils.getSecurity(scanResult);

        if (security.equals(ConnectorUtils.OPEN))
        {
            final int numOpenNetworksKept = 10;
            checkForExcessOpenNetworkAndSave(wifiMgr, numOpenNetworksKept);
        }

        WifiConfiguration config = new WifiConfiguration();
        config.SSID = ConnectorUtils.convertToQuotedString(scanResult.SSID);
        config.BSSID = scanResult.BSSID;
        ConnectorUtils.setupSecurity(config, security, password);

        int id = wifiMgr.addNetwork(config);
        Log.d("WiFi", "Network ID: " + Integer.toString(id));
        if (id == -1)
        {
            return -1;
        }

        if (!wifiMgr.saveConfiguration())
        {
            return -1;
        }
        // We have to retrieve the WifiConfiguration after save
        config = ConnectorUtils.getWifiConfiguration(wifiMgr, scanResult, security);
        if (config == null)
        {
            return -1;
        }

        return connectToConfiguredNetwork(wifiMgr, config, true);
    }

    public static int connectToConfiguredNetwork(final WifiManager wifiMgr, WifiConfiguration config, boolean reassociate)
    {
        final String security = ConnectorUtils.getSecurity(config);
        int oldPri = config.priority;

        // Make it the highest priority.
        int newPri = ConnectorUtils.getMaxPriority(wifiMgr) + 1;
        if(newPri > MAX_PRIORITY) {
            newPri = ConnectorUtils.shiftPriorityAndSave(wifiMgr);
            config = ConnectorUtils.getWifiConfiguration(wifiMgr, config, security);
            if(config == null) {
                return -1;
            }
        }

        // Set highest priority to this configured network
        config.priority = newPri;
        int networkId = wifiMgr.updateNetwork(config);
        if (networkId == -1)
        {
            return -1;
        }

        // Do not disable others
        if(!wifiMgr.enableNetwork(networkId, false)) {
            config.priority = oldPri;
            return -1;
        }

        if(!wifiMgr.saveConfiguration()) {
            Log.d(TAG,"COULDN'T SAVE THE CONFIGURATION");
            config.priority = oldPri;
            return -1;
        }

        // We have to retrieve the WifiConfiguration after save.
        config = ConnectorUtils.getWifiConfiguration(wifiMgr, config, security);

        if(config == null) {
            return -1;
        }
        // Disable others, but do not save.
        // Just to force the WifiManager to connect to it.
        if (!wifiMgr.enableNetwork(config.networkId, true))
        {
            return -1;
        }

        final boolean connect = reassociate ? wifiMgr.reassociate() : wifiMgr.reconnect();
        if (!connect)
        {
            return -1;
        }
        return config.networkId;
    }

    private static boolean checkForExcessOpenNetworkAndSave(final WifiManager wifiMgr, final int numOpenNetworksKept)
    {
        final List<WifiConfiguration> configurations = wifiMgr.getConfiguredNetworks();
        ConnectorUtils.sortByPriority(configurations);

        boolean modified = false;
        int tempCount = 0;
        for (int i = configurations.size() - 1; i >= 0; i--)
        {
            final WifiConfiguration config = configurations.get(i);
            if (ConnectorUtils.getSecurity(config).equals(ConnectorUtils.OPEN))
            {
                tempCount++;
                if (tempCount >= numOpenNetworksKept)
                {
                    modified = true;
                    wifiMgr.removeNetwork(config.networkId);
                }
            }
        }
        return !modified || wifiMgr.saveConfiguration();
    }

    public static void reenableAllHotspots(WifiManager wifi)
    {
        final List<WifiConfiguration> configurations = wifi.getConfiguredNetworks();
        if (configurations != null && !configurations.isEmpty())
        {
            for (final WifiConfiguration config : configurations)
            {
                wifi.enableNetwork(config.networkId, false);
            }
        }
    }
}