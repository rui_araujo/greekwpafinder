package com.Fisherman.Greekwpa.connector;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.AuthAlgorithm;
import android.net.wifi.WifiConfiguration.GroupCipher;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiConfiguration.PairwiseCipher;
import android.net.wifi.WifiConfiguration.Protocol;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.Comparator;
import java.util.List;

public class ConnectorUtils
{
	// Constants used for different security types
	public static final String WPA2 = "WPA2";
	public static final String WPA = "WPA";
	public static final String WEP = "WEP";
	public static final String OPEN = "Open";
	// For EAP Enterprise fields
	public static final String WPA_EAP = "WPA-EAP";
	public static final String IEEE8021X = "IEEE8021X";

	static final String[] SECURITY_MODES = { WEP, WPA, WPA2, WPA_EAP, IEEE8021X };

    /**
     * Fill in the security fields of WifiConfiguration config.
     *
     * @param config
     *            The object to fill.
     * @param security
     *            If is OPEN, password is ignored.
     * @param password
     *            Password of the network if security is not OPEN.
     */
    static void setupSecurity(WifiConfiguration config, String security, final String password)
    {
        config.allowedAuthAlgorithms.clear();
        config.allowedGroupCiphers.clear();
        config.allowedKeyManagement.clear();
        config.allowedPairwiseCiphers.clear();
        config.allowedProtocols.clear();

        if (TextUtils.isEmpty(security))
        {
            security = OPEN;
            Log.w(WiFi.TAG, "Empty security, assuming open");
        }

        if (security.equals(WEP))
        {
            // If password is empty, it should be left untouched
            if (!TextUtils.isEmpty(password))
            {
                config.wepKeys[0] = isHexWepKey(password) ? password : convertToQuotedString(password);
            }

            config.wepTxKeyIndex = 0;

            config.allowedAuthAlgorithms.set(AuthAlgorithm.OPEN);
            config.allowedAuthAlgorithms.set(AuthAlgorithm.SHARED);

            config.allowedKeyManagement.set(KeyMgmt.NONE);

            config.allowedGroupCiphers.set(GroupCipher.WEP40);
            config.allowedGroupCiphers.set(GroupCipher.WEP104);

        }
        else if (security.equals(WPA) || security.equals(WPA2))
        {
            config.allowedGroupCiphers.set(GroupCipher.TKIP);
            config.allowedGroupCiphers.set(GroupCipher.CCMP);

            config.allowedKeyManagement.set(KeyMgmt.WPA_PSK);

            config.allowedPairwiseCiphers.set(PairwiseCipher.CCMP);
            config.allowedPairwiseCiphers.set(PairwiseCipher.TKIP);

            config.allowedProtocols.set(security.equals(WPA2) ? Protocol.RSN : Protocol.WPA);

            // If password is empty, it should be left untouched
            if (!TextUtils.isEmpty(password))
            {
                if (password.length() == 64 && isHexWepKey(password))
                {
                    // Goes unquoted as hex
                    config.preSharedKey = password;
                }
                else
                {
                    // Goes quoted as ASCII
                    config.preSharedKey = convertToQuotedString(password);
                }
            }

        }
        else if (security.equals(OPEN))
        {
            config.allowedKeyManagement.set(KeyMgmt.NONE);
        }
        else if (security.equals(WPA_EAP) || security.equals(IEEE8021X))
        {
            config.allowedGroupCiphers.set(GroupCipher.TKIP);
            config.allowedGroupCiphers.set(GroupCipher.CCMP);
            if (security.equals(WPA_EAP))
            {
                config.allowedKeyManagement.set(KeyMgmt.WPA_EAP);
            }
            else
            {
                config.allowedKeyManagement.set(KeyMgmt.IEEE8021X);
            }
            if (!TextUtils.isEmpty(password))
            {
                config.preSharedKey = convertToQuotedString(password);
            }
        }
    }

	static int getMaxPriority(final WifiManager wifiManager)
	{
		final List<WifiConfiguration> configurations = wifiManager.getConfiguredNetworks();
		int pri = 0;
		for (final WifiConfiguration config : configurations)
		{
			if (config.priority > pri)
			{
				pri = config.priority;
			}
		}
		return pri;
	}
    static int shiftPriorityAndSave(final WifiManager wifiMgr) {
        final List<WifiConfiguration> configurations = wifiMgr.getConfiguredNetworks();
        sortByPriority(configurations);
        final int size = configurations.size();
        for(int i = 0; i < size; i++) {
            final WifiConfiguration config = configurations.get(i);
            config.priority = i;
            wifiMgr.updateNetwork(config);
        }
        wifiMgr.saveConfiguration();
        return size;
    }

	public static WifiConfiguration getWifiConfiguration(final WifiManager wifiMgr, final ScanResult hotspot, String hotspotSecurity)
    {
        if (hotspot == null)
        {
            return null;
        }
        final String ssid = convertToQuotedString(hotspot.SSID);
        if (ssid.length() == 0)
        {
            return null;
        }

        final String bssid = hotspot.BSSID;
        if (bssid == null)
        {
            return null;
        }

        if (hotspotSecurity == null)
        {
            hotspotSecurity = getSecurity(hotspot);
        }

        try
        {
            final List<WifiConfiguration> configurations = wifiMgr.getConfiguredNetworks();

            for (final WifiConfiguration config : configurations)
            {
                if (config.SSID == null || !ssid.equals(config.SSID))
                {
                    continue;
                }
                if (config.BSSID == null || bssid.equals(config.BSSID))
                {
                    final String configSecurity = getSecurity(config);
                    if (hotspotSecurity.equals(configSecurity))
                    {
                        return config;
                    }
                }
            }
        }
        catch (NullPointerException e)
        {
            return null;
        }

		return null;
	}

	public static WifiConfiguration getWifiConfiguration(final WifiManager wifiMgr, final WifiConfiguration configToFind, String security)
	{
        final String ssid = configToFind.SSID;
        if (ssid.length() == 0)
        {
            return null;
        }

        final String bssid = configToFind.BSSID;

		if (security == null)
		{
			security = getSecurity(configToFind);
		}

		final List<WifiConfiguration> configurations = wifiMgr.getConfiguredNetworks();
        if (configurations == null)
            return null;

		for (final WifiConfiguration config : configurations)
		{
			if (config.SSID == null || !ssid.equals(config.SSID))
			{
				continue;
			}
			if (config.BSSID == null || bssid == null || bssid.equals(config.BSSID))
			{
				final String configSecurity = getSecurity(config);
				if (security.equals(configSecurity))
				{
					return config;
				}
			}
		}
		return null;
	}

	/**
	 * @return The security of a given {@link WifiConfiguration}.
	 */
	static public String getSecurity(WifiConfiguration wifiConfig)
	{

		if (wifiConfig.allowedKeyManagement.get(KeyMgmt.NONE))
		{
			// If we never set group ciphers, wpa_supplicant puts all of them.
			// For open, we don't set group ciphers.
			// For WEP, we specifically only set WEP40 and WEP104, so CCMP
			// and TKIP should not be there.
			if (!wifiConfig.allowedGroupCiphers.get(GroupCipher.CCMP) && (wifiConfig.allowedGroupCiphers.get(GroupCipher.WEP40) || wifiConfig.allowedGroupCiphers.get(GroupCipher.WEP104)))
			{
				return WEP;
			}
			else
			{
				return OPEN;
			}
		}
		else if (wifiConfig.allowedProtocols.get(Protocol.RSN))
		{
			return WPA2;
		}
		else if (wifiConfig.allowedKeyManagement.get(KeyMgmt.WPA_EAP))
		{
			return WPA_EAP;
		}
		else if (wifiConfig.allowedKeyManagement.get(KeyMgmt.IEEE8021X))
		{
			return IEEE8021X;
		}
		else if (wifiConfig.allowedProtocols.get(Protocol.WPA))
		{
			return WPA;
		}
		else
		{
			Log.w(WiFi.TAG, "Unknown security type from WifiConfiguration, falling back on open.");
			return OPEN;
		}
	}

	/**
	 * @return The security of a given {@link ScanResult}.
	 */
	public static String getSecurity(ScanResult scanResult)
	{
		final String cap = scanResult.capabilities;
		for (int i = SECURITY_MODES.length - 1; i >= 0; i--)
		{
			if (cap.contains(SECURITY_MODES[i]))
			{
				return SECURITY_MODES[i];
			}
		}

		return OPEN;
	}

    public static String convertToQuotedString(String string) {
        if (TextUtils.isEmpty(string)) {
            return "";
        }

        final int lastPos = string.length() - 1;
        if (lastPos < 0 || (string.charAt(0) == '"' && string.charAt(lastPos) == '"')) {
            return string;
        }

        return "\"" + string + "\"";
    }

    static boolean isHexWepKey(String wepKey)
    {
        final int len = wepKey.length();

        // WEP-40, WEP-104, and some vendors using 256-bit WEP (WEP-232?)
        if (len != 10 && len != 26 && len != 58)
        {
            return false;
        }

        for (int i = wepKey.length() - 1; i >= 0; i--)
        {
            final char c = wepKey.charAt(i);
            if (!(c >= '0' && c <= '9' || c >= 'A' && c <= 'F' || c >= 'a' && c <= 'f'))
            {
                return false;
            }
        }
        return true;
    }


    static void sortByPriority(final List<WifiConfiguration> configurations)
    {
        java.util.Collections.sort(configurations, new Comparator<WifiConfiguration>() {

            @Override
            public int compare(WifiConfiguration object1, WifiConfiguration object2)
            {
                return object1.priority - object2.priority;
            }
        });
    }
}
