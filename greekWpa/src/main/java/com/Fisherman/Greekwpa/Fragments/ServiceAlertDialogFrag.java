package com.Fisherman.Greekwpa.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.Fisherman.Greekwpa.GreekWpaApp;
import com.Fisherman.Greekwpa.R;
import com.Fisherman.Greekwpa.Receivers.ProgressReceiver;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class ServiceAlertDialogFrag extends DialogFragment implements DialogInterface.OnClickListener
{
	private ProgressReceiver.IProgressBar pg;
	private Tracker t;
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		setCancelable(false);
		t = ((GreekWpaApp)activity.getApplication()).getTracker();
		t.setScreenName("ServiceYes/No DialogFragment");
		t.send(new HitBuilders.ScreenViewBuilder().build());
		try
		{
			pg = (ProgressReceiver.IProgressBar)activity;
		}
		catch(ClassCastException e) {}
	}
	public static ServiceAlertDialogFrag newInstance(int title, int message)
	{
		ServiceAlertDialogFrag frag = new ServiceAlertDialogFrag();
		Bundle args = new Bundle();
		args.putInt("title", title);
		args.putInt("text", message);
		frag.setArguments(args);
		return frag;
		
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		int title = getArguments().getInt("title");
		int text = getArguments().getInt("text");
		final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		alert.setTitle(title);
		alert.setMessage(text);
		alert.setCancelable(false);
		alert.setPositiveButton(R.string.yes, this);
		alert.setNegativeButton(R.string.no, this);
		return alert.create();
	}
	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		switch(which)
		{
		case DialogInterface.BUTTON_POSITIVE:
			pg.deactivateProgressBar();
			t.send(new HitBuilders.EventBuilder("onClick", "Service Alert button YES").build());
			break;
		case DialogInterface.BUTTON_NEGATIVE:
			break;
		}
	}

}
