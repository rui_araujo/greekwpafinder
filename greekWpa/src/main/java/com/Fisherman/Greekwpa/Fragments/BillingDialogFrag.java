package com.Fisherman.Greekwpa.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import com.Fisherman.Greekwpa.R;
import com.Fisherman.Greekwpa.billingutil.MyBilling;

public class BillingDialogFrag extends DialogFragment implements DialogInterface.OnClickListener
{

	public static BillingDialogFrag newInstance(int title, int message)
	{
		BillingDialogFrag frag = new BillingDialogFrag();
		Bundle args = new Bundle();
		args.putInt("title", title);
		args.putInt("text", message);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		int title = getArguments().getInt("title");
		int text = getArguments().getInt("text");
		final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		alert.setTitle(title);
		alert.setMessage(text);
		alert.setCancelable(false);
		alert.setPositiveButton(R.string.btn_billing, this);
		return alert.create();
	}

	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		Toast.makeText(getActivity(), R.string.pleaseWait, Toast.LENGTH_LONG).show();
		
		if (which == DialogInterface.BUTTON_POSITIVE)
		{
			MyBilling billing = new MyBilling(getActivity());
			billing.initializeBilling();
		}
	}
	

}
