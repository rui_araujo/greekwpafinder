package com.Fisherman.Greekwpa.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.text.ClipboardManager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.Fisherman.Greekwpa.GreekWpaApp;
import com.Fisherman.Greekwpa.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

@SuppressWarnings("deprecation")
public class KeyTextFragment extends Fragment implements View.OnTouchListener, View.OnClickListener
{
	private TextView lblKey;

	public static KeyTextFragment newInstance(String results)
	{
		KeyTextFragment f = new KeyTextFragment();
		Bundle bdl = new Bundle();
		bdl.putString("results", results);
		f.setArguments(bdl);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		lblKey = new TextView(getActivity());
		lblKey.setOnTouchListener(this);
		lblKey.setOnClickListener(this);
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.CENTER;
		lblKey.setGravity(Gravity.CENTER);
		lblKey.setLayoutParams(params);
		lblKey.setClickable(true);
		lblKey.setTextColor(Color.RED);
		lblKey.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 30);
		lblKey.setText(getArguments().getString("results"));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return lblKey;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		Tracker t = ((GreekWpaApp) getActivity().getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder("onTouch", "KeyTextFragment").build());
		final Rect rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());

		switch (event.getAction())
		{
		case MotionEvent.ACTION_DOWN:
			lblKey.setTextColor(Color.BLUE);
			break;
		case MotionEvent.ACTION_UP:
			if (rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY()))
			{
				v.performClick();
			}
			lblKey.setTextColor(Color.RED);
			break;
		case MotionEvent.ACTION_CANCEL:
			lblKey.setTextColor(Color.RED);
			break;
		case MotionEvent.ACTION_MOVE:
			if (!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY()))
			{
				lblKey.setTextColor(Color.RED);
			}

			break;
		}
		return true;
	}

	@Override
	public void onClick(View v)
	{
		final ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
		clipboard.setText(lblKey.getText());
		startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
		Toast.makeText(getActivity(), R.string.toastCopied, Toast.LENGTH_LONG).show();
	}

}
