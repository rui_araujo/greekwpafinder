package com.Fisherman.Greekwpa.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;

import com.Fisherman.Greekwpa.R;

public class AutoConnectButtonFragment extends Fragment implements OnClickListener
{
	private Button btnConnect;
	public interface IAutoConnect
	{
		public void activateReceiver();
	}
	IAutoConnect autoConnect;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		btnConnect = new Button(getActivity(), null, android.R.attr.buttonStyleSmall);
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.CENTER;
		params.setMargins(0, 5, 0, 0);
		
		btnConnect.setLayoutParams(params);
		btnConnect.setText(getResources().getString(R.string.autoConnect));
		btnConnect.setOnClickListener(this);
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		autoConnect = (IAutoConnect) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return btnConnect;
	}

	@Override
	public void onClick(View v)
	{ 
		autoConnect.activateReceiver();
	}

}
