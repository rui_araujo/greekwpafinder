package com.Fisherman.Greekwpa.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.WindowManager;

import com.Fisherman.Greekwpa.GreekWpaApp;
import com.Fisherman.Greekwpa.MessageHandler;
import com.Fisherman.Greekwpa.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class ProgressDialogFrag extends DialogFragment
{
	private MessageHandler.IActivityHandler handleStop;
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		Tracker t = ((GreekWpaApp)activity.getApplication()).getTracker();
		t.setScreenName("Thomson Progress DialogFragment");
		t.send(new HitBuilders.ScreenViewBuilder().build());
		try
		{
			handleStop = (MessageHandler.IActivityHandler) activity;
			setCancelable(true);
		}
		catch (ClassCastException e) {}
	}

	public static ProgressDialogFrag newInstance(int title,String text)
	{
		ProgressDialogFrag frag = new ProgressDialogFrag();
		Bundle args = new Bundle();
		args.putInt("title", title);
		args.putString("text", text);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		int title = getArguments().getInt("title");
		String text = getArguments().getString("text");
		final ProgressDialog progressDiag = new ProgressDialog(getActivity());
		progressDiag.setTitle(title);
		progressDiag.setMessage(text);
		progressDiag.setIndeterminate(true);
		progressDiag.setCancelable(true);
		progressDiag.setButton(DialogInterface.BUTTON_NEGATIVE, getActivity().getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if (progressDiag != null && progressDiag.isShowing())
				{
					handleStop.doError();
				}
				
			}
		});
		progressDiag.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		return progressDiag;
	}

	@Override
	public void onCancel(DialogInterface dialog)
	{
		super.onCancel(dialog);
		Log.i("ProgressDialogFrag", "DIALOG CANCELED by onCancel METHOD");
		handleStop.doError();
		
	}
	

}
