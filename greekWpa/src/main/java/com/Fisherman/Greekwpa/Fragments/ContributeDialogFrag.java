package com.Fisherman.Greekwpa.Fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.Fisherman.Greekwpa.Integrity;
import com.Fisherman.Greekwpa.MyPreferences;
import com.Fisherman.Greekwpa.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class ContributeDialogFrag extends DialogFragment implements View.OnClickListener
{
	private CheckBox checkSave;
	private InterstitialAd ad;

	public static ContributeDialogFrag newInstance(int title, int message)
	{
		ContributeDialogFrag frag = new ContributeDialogFrag();
		Bundle bdl = new Bundle();
		bdl.putInt("message", message);
		bdl.putInt("title", title);
		frag.setArguments(bdl);
		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NORMAL, 0);
		setCancelable(false);
		ad = new InterstitialAd(getActivity());
		ad.setAdUnitId(Integrity.INTERSTITIAL_ID);
		ad.loadAd(new AdRequest.Builder().build());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.contribute_dialog_frag, container, false);
		checkSave = (CheckBox) v.findViewById(R.id.check_save);
		TextView txtContribute = (TextView) v.findViewById(R.id.txt_contribute);
		Button btnContribute = (Button) v.findViewById(R.id.btn_contribute);
		btnContribute.setOnClickListener(this);
		int message = getArguments().getInt("message");
		int title = getArguments().getInt("title");
		getDialog().setTitle(title);
		txtContribute.setText(message);
		if ((Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB))
		{
			checkSave.setTextColor(Color.CYAN);
		}
		return v;
	}

	@Override
	public void onStart()
	{
		super.onStart();
		Dialog diag = getDialog();
		// Fills up the entire Screen
		if (diag != null)
		{
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(diag.getWindow().getAttributes());
			// This makes the dialog take up the full width
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
			diag.getWindow().setAttributes(lp);
		}
	}

	@Override
	public void onClick(View v)
	{
		MyPreferences prefs = new MyPreferences(getActivity());
		if (checkSave.isChecked())
			prefs.putBlockDialogContribute();
		if (ad.isLoaded())
			ad.show();
		dismiss();
	}

}
