package com.Fisherman.Greekwpa.explore;


import android.os.AsyncTask;

import com.Fisherman.Greekwpa.models.VenueSearchRoot;

public class AsyncExploreController extends AsyncTask<Double, Void, VenueSearchRoot>
{
    private final IAsyncSearchExplore<VenueSearchRoot> listener;

    public AsyncExploreController(IAsyncSearchExplore<VenueSearchRoot> listener)
    {
        this.listener = listener;
    }

    @Override
    protected VenueSearchRoot doInBackground(Double... params)
    {
        VenueSearchRoot vroot;
        vroot = new ExploreGuestController().controlGuestExplore(params[0], params[1], ExploreGuestDao.RADIUS);

        if (vroot == null)
            vroot = new ExploreGuestController().controlGuestExplore(params[0], params[1]);

        return vroot;
    }

    @Override
    protected void onPostExecute(VenueSearchRoot result)
    {
        super.onPostExecute(result);
        listener.onExploreCompleted(result);
    }

    public interface IAsyncSearchExplore<T>
    {
        public void onExploreCompleted(T result);
    }
}
