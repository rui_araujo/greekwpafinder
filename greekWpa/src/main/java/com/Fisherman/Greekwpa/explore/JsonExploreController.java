package com.Fisherman.Greekwpa.explore;


import com.Fisherman.Greekwpa.models.Location;
import com.Fisherman.Greekwpa.models.Tip;
import com.Fisherman.Greekwpa.models.Venue;
import com.Fisherman.Greekwpa.models.VenueSearchRoot;
import com.esotericsoftware.jsonbeans.Json;
import com.esotericsoftware.jsonbeans.JsonReader;
import com.esotericsoftware.jsonbeans.JsonValue;
import com.esotericsoftware.jsonbeans.OutputType;

import java.util.ArrayList;
import java.util.Locale;

public class JsonExploreController
{
    public VenueSearchRoot createRootModel(String jsonString)
    {
        Json json = new Json(OutputType.json);

        JsonReader reader = new JsonReader();
        JsonValue map;
        VenueSearchRoot vroot = new VenueSearchRoot();
        ArrayList<Venue> venues = new ArrayList<>();
        Location loc;
        int meta = reader.parse(jsonString).get("meta").child().asInt();
        if (meta != 200) return null;
        map = reader.parse(jsonString).get("response").getChild("groups");
        for (JsonValue entry = map.getChild("items"); entry != null; entry = entry.next)
        {
            ArrayList<Tip> tips = new ArrayList<>();
            Venue v = new Venue();
            loc = json.readValue(Location.class, entry.get("venue").get("location"));
            v.setId(entry.get("venue").getString("id"));
            v.setName(entry.get("venue").getString("name"));
            v.setLocation(loc);
            for (JsonValue tip = entry.getChild("tips"); tip != null; tip = tip.next())
            {
                String tipId = tip.getString("id");
                String singleTip = tip.getString("text");
                if (singleTip.toLowerCase(Locale.getDefault()).contains("wifi") || singleTip.toLowerCase(Locale.getDefault()).contains("wi-fi"))
                {
                    tips.add(new Tip(tipId, singleTip));
                }
            }
            if (!tips.isEmpty())
            {
                v.setTips(tips);
                venues.add(v);
            }

        }
        vroot.setVenues(venues);

        return vroot;
    }

}
