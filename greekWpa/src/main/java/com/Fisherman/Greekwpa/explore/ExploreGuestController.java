package com.Fisherman.Greekwpa.explore;


import com.Fisherman.Greekwpa.models.VenueSearchRoot;
import com.github.kevinsawicki.http.HttpRequest;

public class ExploreGuestController
{

    public VenueSearchRoot controlGuestExplore(double lat, double lng)
    {

        String result;
        if (lat < -80 || lat > 80) return null;
        if (lng < -180 || lng > 180) return null;

        try
        {
            result = new ExploreGuestDao().exploreVenues(lat, lng);
        }
        catch (HttpRequest.HttpRequestException e)
        {
            e.printStackTrace();
            return null;
        }
        if (result == null || result.isEmpty())
        {
            return null;
        }

        VenueSearchRoot vroot = new JsonExploreController().createRootModel(result);
        if (vroot == null || vroot.getVenues().isEmpty()) return null;
        return vroot;

    }

    public VenueSearchRoot controlGuestExplore(double lat, double lng, int radius)
    {

        String result;
        if (lat < -80 || lat > 80) return null;
        if (lng < -180 || lng > 180) return null;

        try
        {
            result = new ExploreGuestDao().exploreVenues(lat, lng, radius);
        }
        catch (HttpRequest.HttpRequestException e)
        {
            e.printStackTrace();
            return null;
        }
        if (result == null || result.isEmpty())
        {
            return null;
        }

        VenueSearchRoot vroot = new JsonExploreController().createRootModel(result);
        if (vroot == null || vroot.getVenues().isEmpty()) return null;
        return vroot;

    }
}
