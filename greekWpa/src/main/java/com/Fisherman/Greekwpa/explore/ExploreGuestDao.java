package com.Fisherman.Greekwpa.explore;

import com.github.kevinsawicki.http.HttpRequest;

public class ExploreGuestDao
{
    public static final int RADIUS = 1000;
    private static final String DATE = "20140805";
    private static final String CLIENT_ID = "TDCEU3UQXCRUM4LAJBNFM1ICLMSEWXV3Z5UF2VNLQB5GWLW5";
    private static final String CLIENT_SECRET = "4Y122FBSS0CUVC4Y32LPSDNQMSB4COT3CEYXMRLVUL4QJJR5";
    private static final int TIMEOUT_PERIOD = 8000;
    private static final int LIMIT_VENUES = 50;

    public String exploreVenues(double lat, double lng) throws HttpRequest.HttpRequestException
    {
        final String exploreVenues = "https://api.foursquare.com/v2/venues/explore?v=" + DATE + "&sortByDistance=1" + "&query=wifi" + "&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&limit=" + LIMIT_VENUES;
        String latLng = "&ll=" + lat + "," + lng;

        String exploreQuery = exploreVenues + latLng;
        return HttpRequest.get(exploreQuery).accept("application/json").body();
    }

    public String exploreVenues(double lat, double lng, int radius)
            throws HttpRequest.HttpRequestException
    {
        final String exploreVenues = "https://api.foursquare.com/v2/venues/explore?v=" + DATE + "&sortByDistance=1" + "&query=wifi" + "&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&limit=" + LIMIT_VENUES + "&radius=" + radius;
        String latLng = "&ll=" + lat + "," + lng;
        String exploreQuery = exploreVenues + latLng;
        return HttpRequest.get(exploreQuery).accept("application/json").body();
    }

}
