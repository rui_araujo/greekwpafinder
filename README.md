# Greek WPA Finder for Android #

This app was originally intended to be used inside Greece as it can recover default WPA/WEP Keys of specific router brands that are popular in Greece thus helping you test your network security. Hopefully this will change and more routers will be supported from around the world soon (name should change too when this happens).

### Which types of routers are supported? ###

Currently the following brands are supported:

* CYTA Pirelli
* Some CYTA Adb
* Some CYTA ZTE
* NetFaster Hol Intracom
* Some OTE Huawei-Askey
* Some OTE ZTE (conn-xXXXXXX)
* Some OTE BAUDTEC
* Some DLink
* Thomson/SpeedTouch/Infinitum/CYTAXXXXXX Thanks to [Router Keygen](https://github.com/routerkeygen)
* To Be continued hopefully...

### This project is licensed under [GPL](http://www.gnu.org/copyleft/gpl.html) ###

# **Greek WPA Finder is available on** [**Google Play**](https://play.google.com/store/apps/details?id=com.Fisherman.Greekwpa) #

### Contact info: ###

My name is Thanos Psaridis (also known as Thanos Fisherman). You can find me on:

* [LinkedIn](https://www.linkedin.com/in/thanosfish)
* [GitHub](https://github.com/ThanosFisherman)
* My personal base64 encrypted mail: (needs decoding first :P) *cHNhcmlkaXNAZ21haWwuY29t*